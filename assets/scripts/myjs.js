function getRequest(url, data, success, error) {
    $.ajax({
        url : url,
        data : data,
        type : 'GET',
        success : success,
        error : error
    });
}

function postRequest(url, data, success, error) {
    $.ajax({
        url : url,
        data : data,
        type : 'POST',
        success : success,
        error : error
    });
}

function alert(alert,message) {
    // let alert = $('#alert-success');
    $('#'+alert).find('#message').text(message);
    $('#'+alert).slideToggle(600,function () {
        setTimeout(function () {
            $('#'+alert).slideToggle(600);
        },10000);
    });
}

// $('.divide').keyup(function(event) {

//     // skip for arrow keys
//     if(event.which >= 37 && event.which <= 40) return;

//     // format number
//     $(this).val(function(index, value) {
//         return value
//         .replace(/\D/g, "")
//         .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
//         ;
//     });
// });

$('.divide').divide({
    delimiter: '.',
    divideThousand: true, // 1,000..9,999
    delimiterRegExp: /[\.\,\s]/g
});

const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function loading(id) {
    $('#'+id).toggle();
}

function initDataTable(id) {
    if (id instanceof Array) {
        for (let i = 0; i < id.length; i++) {
            const x = id[i];
            $('#'+x).DataTable({
                responsive: true, 
                scrollX: true,
                paginate: true,
                "columnDefs": [
                  {"orderable": false, "targets": [0] }
                ],
                "aaSorting": []
            });
        }
    } else {
        return $('#'+id).DataTable({
            responsive: true, 
            scrollX: true,
            paginate: true,
            "columnDefs": [
              {"orderable": false, "targets": [0] }
            ],
            "aaSorting": []
        });
    }
}

function strContains(str1, str2) {
    if(str1.indexOf(str2) != -1){
        return true;
    }
    return false;
}

$(document).ready(function ($) {

    let menus = $('#sidebar-menu li')

    $('#sidebar-search').keyup(function (event) {
        str2 = event.currentTarget.value;
        menus.map(
            function(x) {
                y = menus[x];
                console.log();
                if(strContains(y.className,str2)){
                    console.log(y);
                    $('.'+y.className).show();
                }
                else{
                    console.log('NOPE')
                    $('.'+y.className).hide();
                }
            }
        );
        // console.log(event.currentTarget.value);
        // console.log(menus);
    })
});