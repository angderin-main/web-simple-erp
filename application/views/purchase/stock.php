<!DOCTYPE html>
<html>
<head>
  <?php echo $head; ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <?php echo $header; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php echo $sidebar ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sales
        <small>History</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i>Penjualan</a></li>
        <li class="active">List Penjualan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-11">
          <div class="col-md-2">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Menu</h3>
                <a href="<?php echo base_url('stock') ?>"><button id="add-stock" class="btn btn-primary btn-block" ><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</button></a>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <!-- <button id="add-product" class="btn btn-primary btn-block" >Tambah produk baru</button> -->
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
          <div class="col-md-10">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Tabel Stock</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <table id="product-table" class="table table-bordered table-striped" style="width:100%">
                  <thead>
                    <th>No</th>
                    <th>Code</th>
                    <th>Penerima</th>
                    <th>Supplier</th>
                    <th>Catatan</th>
                    <th>Pembuatan</th>
                    <th>Detail</th>
                  </thead>
                  <tbody id="product-list">
                  </tbody>
                </table>
              </div><!-- /.box-body -->
              <div id="loadingSpinner" class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div><!-- /.box -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php echo $footer ?>

  <!-- Control Sidebar -->
  <?php echo $sidebar_control ?>
</div>
<!-- ./wrapper -->

<?php echo $scripts; ?>

<script type="text/javascript">
  $(document).ready(function($) {

    populateSaleList();

    loading();

    function populateSaleList() {

      let url = "<?php echo base_url('Purchase/getstockp'); ?>";
      let data = {};
      
      let success = (data)=>{
        console.log(data);
        $("#product-table").dataTable().fnDestroy();
        $('#product-list').empty();
        if (!data.message){
          $('#product-table').DataTable({
            responsive: true, 
            scrollX: true,
            paginate: true,
            "columnDefs": [
              {"orderable": false, "targets": [6] }
            ],
            "aaSorting": []
          });
          loading();
          return;
        }

        const numberWithCommas = (x) => {
          return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        for (var i = 0; i < data.message.length; i++) {
          let x = data.message[i];
          let row = `
          <tr>
            <td>`+(i+1)+`</td>
            <td>`+x.code+`</td>
            <td>`+x.createdby+`</td>
            <td>`+x.supplier_name+`</td>
            <td>`+x.note+`</td>  
            <td>`+x.createdat+`</td>
            <td style='text-align:center;'>
              <a href="<?php echo base_url('purchasestockdetail/') ?>`+x.id+`" targets="_blank"><button id_item=`+x.id+` type="button" class="btn btn-primary"><i class="fa fa-search"></i> Detail</button></a>
            </td>
          </tr>
          `;
          
          $('#product-list').append(row);
        }

        $('#product-table').DataTable({
          responsive: true, 
          scrollX: true,
          paginate: true,
          "columnDefs": [
            {"orderable": false, "targets": [6] }
          ],
          "aaSorting": []
        });

        loading();
      }

      let error = (data)=>{
        console.log(data);
        alert('Unknown Error');
        loading();
      }

      loading();
      getRequest(url, data, success, error);
    }

    function loading() {
      $('#loadingSpinner').toggle();
    }

    function getRequest(url, data, success, error) {
      $.ajax({
        url : url,
        data : data,
        type : 'GET',
        success : success,
        error : error
      });
    }

    function postRequest(url, data, success, error) {
      $.ajax({
        url : url,
        data : data,
        type : 'POST',
        success : success,
        error : error
      });
    }

  })
</script>
</body>
</html>