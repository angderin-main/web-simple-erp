<!DOCTYPE html>
<html>
<head>
  <?php echo $head; ?>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
<div class="wrapper">

  <!-- Main Header -->
  <?php echo $header; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php echo $sidebar ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penjualan
        <small>Detail Penjualan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('history'); ?>"><i class="fa fa-dashboard"></i>Penjualan</a></li>
        <li class="active">Detail Penjualan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-11">
          <div class="col-md-2">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Menu</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <a href="<?php echo base_url('purchasestock') ?>"><button id="add-product" class="btn btn-primary btn-block" ><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</button></a>
                  </div>
                </div>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
          <div class="col-md-10">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Detail Pembelian</h3>
                <input id="pid" type="hidden" value="<?php echo $pid; ?>">
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-3">
                    <div class="col-md-12">
                      <label class="control-label" for="scode">Kode</label>
                      <input id="scode" class="form-control" type="text" value="" readonly>
                    </div>
                    <div class="col-md-12">
                      <label class="control-label" for="sdate">Pembelian</label>
                      <input id="sdate" class="form-control" type="text" value="" readonly>
                    </div>
                  </div>
                  <div class="col-md-5 border-left border-right">
                    <div class="col-md-6">
                      <label class="control-label" for="scname">Supplier</label>
                      <input id="scname" class="form-control" type="text" value="" readonly>
                    </div>
                    <div class="col-md-6">
                      <label class="control-label" for="scphone">Telepon</label>
                      <input id="scphone" class="form-control" type="text" value="" readonly>
                    </div>
                    <div class="col-md-12">
                      <label class="control-label" for="scaddress">Alamat</label>
                      <input id="scaddress" class="form-control" type="text" value="" readonly>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="col-md-12">
                      <label class="control-label" for="screatedby">Penerima</label>
                      <input id="screatedby" class="form-control" type="text" value="" readonly>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-12">
                    <label for="salesproduct-table">List Barang</label>
                    <table id="salesproduct-table" class="table table-bordered table-striped" style="width:100%">
                      <thead>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Satuan</th>
                        <th>Deskripsi</th>
                        <th>Stok Awal</th>
                        <th>Stok Masuk</th>
                        <th>Stok Akhir</th>
                        <th>Subtotal</th>
                      </thead>
                      <tbody id="salesproduct-list">
                      </tbody>
                    </table>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-12">
                    <label class="control-label" for="snote">Note</label>
                    <textarea id="snote" class="form-control" type="text" value="" readonly></textarea>
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div id="loadingSpinner" class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div><!-- /.box -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php echo $footer ?>

  <!-- Control Sidebar -->
  <?php echo $sidebar_control ?>
</div>
<!-- ./wrapper -->

<?php echo $scripts; ?>

<script type="text/javascript">
  $(document).ready(function($) {

    populateSalesProduct();
    populateSalesDetail();

    loading();

    function populateSalesProduct() {

      let pid = $('#pid').val();

      let url = "<?php echo base_url('Purchase/getstockproductp'); ?>";
      let data = {pid : pid};
      
      let success = (data)=>{
        console.log(data);
        $("#salesproduct-table").dataTable().fnDestroy();
        $('#salesproduct-list').empty();
        if (!data.message){
          $('#salesproduct-table').DataTable({
            responsive: true, 
            scrollX: true,
            paginate: true,
            "columnDefs": [
            ],
            "aaSorting": []
          });
          loading();
          return;
        }

        const numberWithCommas = (x) => {
          return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        for (var i = 0; i < data.message.length; i++) {
          let x = data.message[i];
          let row = `
          <tr>
            <td>`+(i+1)+`</td>
            <td>`+x.pname+`</td>
            <td>`+x.punit+`</td>
            <td>`+x.pdesc+`</td>
            <td>`+x.start+`</td>
            <td>`+x.in+`</td>
            <td>`+x.final+`</td>
            <td>Rp. `+numberWithCommas(x.subtotal)+`</td>
          </tr>
          `;

          $('#salesproduct-list').append(row);
        }

        $('#salesproduct-table').DataTable({
          responsive: true, 
          scrollX: true,
          paginate: true,
          "columnDefs": [
          ],
          "aaSorting": []
        });

        loading();
      }

      let error = (data)=>{
        console.log(data);
        alert('Unknown Error');
        loading();
      }

      loading();
      getRequest(url, data, success, error);
    }

    function populateSalesDetail() {
      let pid = $('#pid').val();

      let url = "<?php echo base_url('Purchase/getstockdetailp'); ?>";
      let data = {pid : pid};
      
      let success = (data)=>{

        const numberWithCommas = (x) => {
          return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        let s = data.message;
        
        $('#scode').val(s.code);
        $('#sdate').val(s.createdat);
        $('#scname').val(s.supplier_name);
        $('#scphone').val(s.supplier_phone);
        $('#scaddress').val(s.supplier_address);
        $('#screatedby').val(s.createdby);
        $('#snote').val(s.note);

        loading();
      }

      let error = (data)=>{
        console.log(data);
        alert('Unknown Error');
        loading();
      }

      loading();
      getRequest(url, data, success, error);
    }

    function loading() {
      $('#loadingSpinner').toggle();
    }

    function getRequest(url, data, success, error) {
      $.ajax({
        url : url,
        data : data,
        type : 'GET',
        success : success,
        error : error
      });
    }

    function postRequest(url, data, success, error) {
      $.ajax({
        url : url,
        data : data,
        type : 'POST',
        success : success,
        error : error
      });
    }

  })
</script>
</body>
</html>