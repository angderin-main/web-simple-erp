<!DOCTYPE html>
<html>
<head>
  <?php echo $head; ?>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
<div class="wrapper">

  <!-- Main Header -->
  <?php echo $header; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php echo $sidebar ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penjualan
        <small>List Harga</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('sales') ?>"><i class="fa fa-dashboard"></i> Sales</a></li>
        <li class="active">Kasir</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-12">
            
          </div>
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Pelanggan</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <form id="cform" class="form" role="form">
                  <div class="row">
                    <div class="col-md-9">
                      <div class="form-group">
                        <div class="col-md-12 col-xs-12">
                            <label for="cid" class="control-label">Pelanggan langganan</label>
                            <select name="cselect" id="cselect" class="form-control select2">
                                <option value="0" selected>Pilih pelanggan</option>
                                <?php foreach ($customer as $c) :?>
                                <option value="<?php echo $c->id ?>" cname="<?php echo $c->name ?>" cphone="<?php echo $c->phone ?>" caddress="<?php echo $c->address ?>"><?php echo $c->name ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <!-- <div class="col-md-2">
                          <div class="form-group">
                            <label for="caddress" class="control-label">Tipe</label>
                            <select name="type" id="type" class="form-control">
                              <option value="2">Agent</option>
                              <option value="1">Distributor</option>
                            </select>
                          </div>
                        </div> -->
                      </div>
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-primary btn-block" type="button" id="btn-lockc" >Kunci pelanggan</button>
                        <button class="btn btn-warning btn-block" disabled="disabled" type="button" id="btn-resetc">Reset pelanggan</button>
                    </div>
                  </div>
                </form>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
          <div class="col-md-12 col-xs-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Tabel Product</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <table id="product-table" class="table table-bordered table-striped" style="width:100%">
                  <thead>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Price</th>
                    <th>Unit</th>
                    <th>Desc</th>
                    <th>Stock</th>
                  </thead>
                  <tbody id="product-list">
                  </tbody>
                </table>
              </div><!-- /.box-body -->
              <div id="loadingSpinner" class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div><!-- /.box -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php echo $footer ?>

  <!-- Control Sidebar -->
  <?php echo $sidebar_control ?>
</div>
<!-- ./wrapper -->

<?php echo $scripts; ?>
<script>
  $(document).ready(function($){
    $('.select2').select2();

    let base_url = "<?php echo base_url(); ?>";

    let table = initDataTable('product-table');

    loading('loadingSpinner');

    $('#cselect').change(function () {
      let customer = $('#cselect').find(":selected");
      if (customer.val()!=0) {
        lockc();
      } else {
        resetc();
      }
    });

    $('#btn-lockc').click(function(){
      lockc();
    });

    $('#btn-resetc').click(function(){
      resetc();
    });

    function lockc() {
      $('#cselect').prop('disabled',true);
      $('#btn-resetc').prop('disabled',false);
      $('#btn-lockc').prop('disabled',true);

      let cid = $('#cselect').val();
      populatelistproduct(cid);
    }

    function resetc() {
      $('#cselect').prop('disabled',false);
      $('#btn-resetc').prop('disabled',true);
      $('#btn-lockc').prop('disabled',false);
      $("#cselect").prop('selectedIndex',0);
      $('#cselect').select2();

      table.destroy();
      $('#product-list').empty();
      table = initDataTable('product-table');
    }

    function populatelistproduct (cid) {
      // console.log(cid);
      let type = 0;
      let url = base_url+"/sales/getProduct";
      let data = {cid : cid, type: type};

      let success = (data)=>{
        // console.log(data);
        table.destroy();
        let product = data.message;
        $('#product-list').empty();
        for (let i = 0; i < product.length; i++) {
          let x = product[i];
          let row = `
          <tr>
            <td>`+(i+1)+`</td>
            <td>`+x.name+`</td>
            <td>`+x.price+`</td>
            <td>`+x.unit+`</td>
            <td>`+x.desc+`</td>
            <td>`+x.stock+`</td>
          </tr>
          `;
          $('#product-list').append(row);
        }
        table = initDataTable('product-table');
        // $('.btn-pqty').click(function() {
        //   let cid = $('#cselect').val();
        //   let pqty = $(this).siblings('#pqty').val();
        //   let pid = $(this).attr('pid');
        //   console.log(pqty);
        //   addCart(pid,pqty,cid);
        // });

        loading('loadingSpinner');
      }

      let error = (data)=>{

      }

      loading('loadingSpinner');

      getRequest(url,data,success,error);
    }

  })
</script>

</body>
</html>