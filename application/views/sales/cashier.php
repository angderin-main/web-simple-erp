<!DOCTYPE html>
<html>
<head>
  <?php echo $head; ?>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
<div class="wrapper">

  <!-- Main Header -->
  <?php echo $header; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php echo $sidebar ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penjualan
        <small>Kasir</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('sales') ?>"><i class="fa fa-dashboard"></i> Sales</a></li>
        <li class="active">Kasir</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-12">
            
          </div>
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Pelanggan</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <form id="cform" class="form" role="form">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="cid" class="control-label">Pelanggan langganan</label>
                        <select name="cselect" id="cselect" class="form-control select2">
                          <option value="0" selected>Pilih pelanggan</option>
                          <?php foreach ($customer as $c) :?>
                          <option value="<?php echo $c->id ?>" cname="<?php echo $c->name ?>" cphone="<?php echo $c->phone ?>" caddress="<?php echo $c->address ?>"><?php echo $c->name ?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="cname" class="control-label">Nama</label>
                        <input name="cname" type="text" class="form-control" id="cname" placeholder="Nama pelanggan" value="Guest">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="cphone" class="control-label">Telepon</label>
                        <input name="cphone" type="text" class="form-control" id="cphone" placeholder="Telepon" value="0812345678">
                      </div>  
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="caddress" class="control-label">Alamat</label>
                        <input name="caddress" type="text" class="form-control" id="caddress" placeholder="Alamat" value="Guest address">
                      </div>
                    </div>
                    <div class="col-md-1">
                      <div class="form-group">
                        <label for="caddress" class="control-label">Tipe</label>
                        <select name="type" id="type" class="form-control">
                          <option value="2">Agent</option>
                          <option value="1">Distributor</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <button class="btn btn-primary btn-block" type="button" id="btn-lockc" >Kunci pelanggan</button>
                      <button class="btn btn-warning btn-block" disabled="disabled" type="button" id="btn-resetc">Reset pelanggan</button>
                    </div>
                  </div>
                </form>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
          <div class="col-md-8">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Tabel Product</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <table id="product-table" class="table table-bordered table-striped" style="width:100%">
                  <thead>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Tipe</th>
                    <th>Price</th>
                    <th>Unit</th>
                    <th>Desc</th>
                    <th>Stock</th>
                    <th width="30%" >Menu</th>
                  </thead>
                  <tbody id="product-list">
                  </tbody>
                </table>
              </div><!-- /.box-body -->
              <div id="loadingSpinner" class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div><!-- /.box -->
          </div>
          <div class="col-md-4">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Tabel Keranjang</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <div class="form-group">
                  <label for="cname-checkout" class="control-label">Nama</label>
                  <input name="cname-checkout" type="text" class="form-control cname-checkout" id="cname-checkout" placeholder="Nama pelanggan" value="Guest" readonly>
                </div>
                <table id="cart-table" class="table table-striped table-bordered">
                  <thead>
                    <th class="col-md-1">No</th>
                    <th>Produk</th>
                    <th>Tipe</th>
                    <th class="col-md-3">Price</th>
                    <th class="col-md-2">Qty</th>
                    <th>Subtotal (Rp.)</th>
                    <th>Menu</th>
                  </thead>
                  <tbody id="cart-list-1" class="cart-list">
                    <!-- <tr>
                      <td>1</td>
                      <td>Tissue</td>
                      <td>120</td>
                      <td>10</td>
                      <td>1200</td>
                    </tr> -->
                  </tbody>
                </table>
                <div class="form-group">
                  <label for="total" class="control-label">Total</label>
                  <input name="total" type="text" class="form-control cart-total" id="total" placeholder="Total pembelian" value="0" readonly>
                </div>
                <hr>
                <div class="form-group">
                  <button class="btn btn-primary" type="button" id="btn-checkout">Konfirmasi</button>
                  <button class="btn btn-warning" type="button" id="btn-clear">Hapus Keranjang</button>
                  <select name="disc" id="disc" class="pull-right">
                    <option value="0">0%</option>
                    <option value="0.01">1%</option>
                    <option value="0.02">2%</option>
                    <option value="0.03">3%</option>
                    <option value="0.04">4%</option>
                    <option value="0.05">5%</option>
                    <option value="0.06">6%</option>
                    <option value="0.07">7%</option>
                    <option value="0.08">8%</option>
                    <option value="0.09">9%</option>
                    <option value="0.10">10%</option>
                  </select>
                  <label for="disc" class="pull-right">Potongan Distributor : </label>
                </div>                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="modal-checkout" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Konfirmasi Pembelian</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="cname-checkout" class="control-label">Nama</label>
                <input name="cname-checkout" type="text" class="form-control cname-checkout" id="cname-checkout" placeholder="Nama pelanggan" value="Guest" readonly>
              </div>
              <table id="cart-table" class="table table-striped table-bordered">
                <thead>
                  <th>No</th>
                  <th>Produk</th>
                  <th>Tipe</th>
                  <th>price</th>
                  <th>Qty</th>
                  <th>Subtotal</th>
                </thead>
                <tbody id="cart-list-2" class="checkout-list">
                  <!-- <tr>
                    <td>1</td>
                    <td>Tissue</td>
                    <td>120</td>
                    <td>10</td>
                    <td>1200</td>
                  </tr> -->
                </tbody>
              </table>
              <div class="form-group">
                <label for="total" class="control-label">Total</label>
                <input name="total" type="text" class="form-control checkout-total" id="total" placeholder="Total pembelian" value="0" readonly>
              </div>
              <div class="form-group">
                <label for="paid" class="control-label">Pembayaran</label>
                <input name="paid" type="text" class="divide form-control" id="paid" placeholder="Pembayaran" value="0">
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
              <a id="btn-detail" href="#" class="btn btn-primary disabled" role="button" aria-disabled="true" target="_blank">Detail</a>
              <a id="btn-nota" href="#" class="btn btn-primary disabled" role="button" aria-disabled="true" target="_blank">Nota</a>
              <button id="btn-pay" type="button" class="btn btn-primary">Bayar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php echo $footer ?>

  <!-- Control Sidebar -->
  <?php echo $sidebar_control ?>
</div>
<!-- ./wrapper -->

<?php echo $scripts; ?>
<script>
  $(document).ready(function($){
    $('.select2').select2();

    let base_url = "<?php echo base_url(); ?>";

    let table = initDataTable('product-table');
    // let cart = initDataTable('cart-table');
    clearCart();
    // getCart();

    loading('loadingSpinner');

    $('#cselect').change(function () {
      let customer = $('#cselect').find(":selected");
      if (customer.val()!=0) {
        $('#cname').val(customer.attr('cname'));
        $('.cname-checkout').val(customer.attr('cname'));
        $('#cphone').val(customer.attr('cphone'));
        $('#caddress').val(customer.attr('caddress'));
        // lockc();
      } else {
        // resetc();
      }
    });

    $('#disc').change(function () {
      let disc = $('#disc').find(":selected").val();
      setDisc(disc);
    });

    $('#btn-lockc').click(function(){
      lockc();
    });

    $('#btn-resetc').click(function(){
      resetc();
      clearCart();
    });

    $('#btn-checkout').click(function() {
      $('#modal-checkout').modal();
    });
    
    function openInvoice(sid) {
      window.open(base_url+"Invoice/view/"+sid,'_blank');
    }

    $('#btn-pay').click(function () {
      
      let cid = $('#cselect').val();
      let pay = $('#paid').val();
      let cname = $('#cname').val();
      let cphone = $('#cphone').val();
      let caddress = $('#caddress').val();
      let type = $('#type').val();

      // let total = $('.cart-toal').val();
      // if(pay<total){
      //   if(!confirm('Pembayaran secara kredit?'))return;
      // }

      let url = base_url+"sales/salep"
      let data = {cid : cid, pay : pay, cname : cname, cphone : cphone, caddress : caddress, type: type};

      console.log(data);

      let success = (data) =>{
        console.log(data);
        let status = data.status;
        console.log('ASDASD');
        if(status==1){
          console.log(data);
          alert(data.message.notif);
          // $('#modal-checkout').modal('hide');
          let cid = $('#cselect').val();
          populatelistproduct(cid);
          clearCart();
          // $('#paid').val(0);
          $('#paid').attr('disabled',true);
          $('#btn-detail').attr('href',base_url+"detail/"+data.message.code);
          $('#btn-detail').removeClass('disabled');
          $('#btn-nota').attr('href',base_url+"printpdf/invoice/"+data.message.code);
          $('#btn-nota').removeClass('disabled');
          $('#btn-pay').attr('disabled',true);
          // openInvoice(data.message.sid);
          // window.open(base_url+"detail/"+data.message.code,'_blank');
          // window.open(base_url+"Invoice/view/"+data.message.sid,'_blank');
          // window.open(base_url+"printpdf/invoice/"+data.message.code,'_blank');
        }else{
          alert(data.message);
        }
        // getCart()
      }

      let error = (data) => {
        alert('qweqwe');
      }

      // console.log(data);
      // alert('Penjualan sukses');
      // $('#modal-checkout').modal('hide');
      // let cid2 = $('#cselect').val();
      // populatelistproduct(cid2);
      // clearCart();          
      // $('#paid').val(0);

      postRequest(url, data, success, error);
      // $(this).disable();
    });

    $('#btn-clear').click(function () {
      if(confirm("Yakin hapus keranjang?"))clearCart();
    });

    function lockc() {
      $('#cselect').prop('disabled',true);
      $('#cname, #cphone, #caddress, #type').prop('disabled', true);
      $('#btn-resetc').prop('disabled',false);
      $('#btn-lockc').prop('disabled',true);

      let cid = $('#cselect').val();
      populatelistproduct(cid);
    }

    function resetc() {
      $('#cselect').prop('disabled',false);
      $('#cname, #cphone, #caddress, #type').prop('disabled', false);
      $('#btn-resetc').prop('disabled',true);
      $('#btn-lockc').prop('disabled',false);
      $('#cname, #cphone, #caddress').val('');
      $("#cselect").prop('selectedIndex',0);
      $('#cselect').select2();

      table.destroy();
      $('#product-list').empty();
      table = initDataTable('product-table');
    }

    function addCart(pid, pprc, pqty, cid) {
      let url = base_url+"sales/addCart"
      let data = {pid : pid, pprc : pprc, pqty : pqty, cid : cid}

      let success = (data) =>{
        console.log(data);
        if(data.status==0)alert(data.message);
        $('.checkout-list').empty();
        $('.checkout-total').val(0);
        $('#btn-detail').addClass('disabled');
        $('#btn-nota').addClass('disabled');
        $('#btn-pay').attr('disabled',false);
        $('#paid').attr('disabled',false);
        getCart()
      }

      let error = (data) => {
        console.log(data);
      }

      postRequest(url, data, success, error);
    }

    function setDisc(disc) {
      let url = base_url+"sales/setDisc"
      let data = {disc : disc}

      let success = (data) =>{
        console.log(data);
        if(data.status==0)alert(data.message);
        getCart()
      }

      let error = (data) => {
        console.log(data);
      }

      postRequest(url, data, success, error);
    }

    function getCart() {
      let url = base_url+"sales/getCart";
      let data = {}

      let success = (data) =>{
        console.log(data);
        $('.cart-list').empty();
        // $('.checkout-list').empty();
        if (!data.message.cart) {
          $('#btn-clear').prop('disabled',true);
          $('#btn-checkout').prop('disabled',true);
          $('.cart-total').val(0);
          return;
        }else{
          $('#btn-clear').prop('disabled',false);
          $('#btn-checkout').prop('disabled',false);
        }
        let cart = data.message.cart;
        for (let i = 0; i < cart.length; i++) {
          let x = cart[i];
          let type = x.options.type==1?'DISTRIBUTOR':'AGEN';
          let row = `
          <tr>
            <td>`+(i+1)+`</td>
            <td id="pid" pid=`+x.id+` >`+x.name+`</td>
            <td>`+type+`</td>
            <td id="col-pprc">
              <input id="pprc" type="number" rowid='`+x.rowid+`' class="form-control cart-prc" value="`+x.options.aprice+`" placeholder="`+x.options.aprice+`">
            </td>
            <td id="col-pqty">
              <input id="pqty" type="number" rowid='`+x.rowid+`' class="form-control cart-qty" value="`+x.qty+`" placeholder="`+x.qty+`">
            </td>
            <td>`+numberWithCommas(x.subtotal)+`</td>
            <td>
              <button rowid='`+x.rowid+`' class="btn btn-warning btn-remove">X</button>
            </td>
          </tr>
          `;
          $('.cart-list').append(row);
        }

        $('.cart-prc').change(function() {
          let cid = $('#cselect').val();
          let pqty = $(this).parent().siblings('#col-pqty').find('#pqty').val();
          let pid = $(this).parent().siblings('#pid').attr('pid');
          let pprc = $(this).val();
          // console.log(pqty);
          addCart(pid,pprc,pqty,cid);
        });

        $('.cart-qty').change(function() {
          let cid = $('#cselect').val();
          let pqty = $(this).val();
          let pid = $(this).parent().siblings('#pid').attr('pid');
          let pprc = $(this).parent().siblings('#col-pprc').find('#pprc').val();
          // console.log(pqty);
          addCart(pid,pprc,pqty,cid);
        });

        for (let i = 0; i < cart.length; i++) {
          let x = cart[i];
          let type = x.options.type==1?'DISTRIBUTOR':'AGEN';
          let row = `
          <tr>
            <td>`+(i+1)+`</td>
            <td>`+x.name+`</td>
            <td>`+type+`</td>
            <td>Rp. `+numberWithCommas(x.price)+`</td>
            <td>`+x.qty+`</td>
            <td>Rp. `+numberWithCommas(x.subtotal)+`</td>
          </tr>
          `;
          $('.checkout-list').append(row);
        }

        $('.cart-total').val(numberWithCommas(data.message.total));
        $('.checkout-total').val(numberWithCommas(data.message.total));
        $('.btn-remove').click(function () {
          removeItem($(this).attr('rowid'));
        })
      }

      let error = (date) => {

      }

      getRequest(url, data, success, error);
    }

    function clearCart() {
      let url = base_url+"sales/clearCart"
      let data = {}

      let success = (data) =>{
        console.log(data);
        getCart();
      }

      let error = (date) => {

      }

      getRequest(url, data, success, error);
    }

    function removeItem(rowid) {
      console.log(rowid);
      let url = base_url+"sales/removeItem"
      let data = {rowid : rowid}

      let success = (data) =>{
        console.log(data);
        getCart();
      }

      let error = (date) => {

      }

      postRequest(url, data, success, error);
    }

    function populatelistproduct (cid) {
      // console.log(cid); 
      let type = $("#type").val();
      let url = base_url+"sales/getProduct";
      let data = {cid : cid, type : type};

      let success = (data)=>{
        // console.log(data);
        table.destroy();
        let product = data.message;
        $('#product-list').empty();
        for (let i = 0; i < product.length; i++) {
          let x = product[i];
          let type = x.type==1?'DISTRIBUTOR':'AGEN';
          let row = `
          <tr>
            <td>`+(i+1)+`</td>
            <td>`+x.name+`</td>
            <td>`+type+`</td>
            <td>
              <input id="pprc" type="number" class="form-control" value="`+x.price+`" placeholder="`+x.price+`">
            </td>
            <td>`+x.unit+`</td>
            <td>`+x.desc+`</td>
            <td>`+x.stock+`</td>
            <td>
              <input id="pqty" type="number" class="form-control" value="0" placeholder="Qty">
              <button pid="`+x.id+`" id="btn-pqty" type="submit" class="btn btn-primary btn-pqty">Tambah</button>
            </td>
          </tr>
          `;
          $('#product-list').append(row);
        }
        
        $('.btn-pqty').click(function() {
          let cid = $('#cselect').val();
          let pqty = $(this).siblings('#pqty').val();
          let pid = $(this).attr('pid');
          let pprc = $(this).parent().parent().find('#pprc').val();
          console.log(pqty);
          addCart(pid,pprc,pqty,cid);
        });
        
        table = initDataTable('product-table');
        loading('loadingSpinner');
      }

      let error = (data)=>{

      }

      loading('loadingSpinner');

      getRequest(url,data,success,error);
    }

  })
</script>

</body>
</html>