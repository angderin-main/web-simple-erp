<!DOCTYPE html>
<html>
<head>
  <?php echo $head; ?>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
<div class="wrapper">

  <!-- Main Header -->
  <?php echo $header; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php echo $sidebar ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penjualan
        <small>Detail Penjualan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('history'); ?>"><i class="fa fa-dashboard"></i>Penjualan</a></li>
        <li class="active">Detail Penjualan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-11">
          <div class="col-md-2">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Menu</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <a href="<?php echo base_url('history') ?>"><button id="add-product" class="btn btn-primary btn-block" ><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</button></a>
                  </div>                  
                  <div class="col-md-12">
                    <br>
                    <a href="<?php echo base_url('printpdf/invoice/'.$code) ?>" target="_blank"><button id="add-product" class="btn btn-primary btn-block" ><i class="fa fa-print" aria-hidden="true"></i> Nota</button></a>
                  </div>
                  <div style="display:none;" id="btn-kredit" class="col-md-12">
                    <br>
                    <!-- <a href="<?php echo base_url('invoice/'.$sid) ?>"><button id="add-product" class="btn btn-primary btn-block" ><i class="fa fa-money" aria-hidden="true"></i> Bayar Tagihan</button></a> -->
                    <button id="add-payment" class="btn btn-primary btn-block" ><i class="fa fa-money" aria-hidden="true"></i> Bayar Tagihan</button>
                  </div>
                </div>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
          <div class="col-md-10">
            <div id="alert-success" style="display:none;" class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-check"></i> Perhatian!</h4>
              <div id="message" ></div>
            </div>
            <div id="alert-danger" style="display:none;" class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
              <div id="message" ></div>
            </div>
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Detail Penjualan</h3>
                <input id="sid" type="hidden" value="<?php echo $sid; ?>">
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-3">
                    <div class="col-md-12">
                      <label class="control-label" for="scode">Kode</label>
                      <input id="scode" class="form-control" type="text" value="INV/2018/01/12/1" readonly>
                    </div>
                    <div class="col-md-12">
                      <label class="control-label" for="sdate">Pembelian</label>
                      <input id="sdate" class="form-control" type="text" value="INV/2018/01/12/1" readonly>
                    </div>
                  </div>
                  <div class="col-md-5 border-left border-right">
                    <div class="col-md-6">
                      <label class="control-label" for="scname">Nama</label>
                      <input id="scname" class="form-control" type="text" value="INV/2018/01/12/1" readonly>
                    </div>
                    <div class="col-md-6">
                      <label class="control-label" for="scphone">Telepon</label>
                      <input id="scphone" class="form-control" type="text" value="INV/2018/01/12/1" readonly>
                    </div>
                    <div class="col-md-12">
                      <label class="control-label" for="scaddress">Alamat</label>
                      <input id="scaddress" class="form-control" type="text" value="INV/2018/01/12/1" readonly>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="col-md-6">
                      <label class="control-label" for="spaid">Potongan Distributor</label>
                      <input id="sdisc" class="form-control" type="text" value="INV/2018/01/12/1" readonly>
                    </div>
                    <div class="col-md-6">
                      <label class="control-label" for="spaid">Pembayaran</label>
                      <input id="spaid" class="form-control" type="text" value="INV/2018/01/12/1" readonly>
                    </div>
                    <div class="col-md-12">
                      <label class="control-label" for="stotal">Total</label>
                      <input id="stotal" class="form-control" type="text" value="INV/2018/01/12/1" readonly>
                    </div>
                    
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-12">
                    <label for="salesproduct-table">List Barang</label>
                    <table id="salesproduct-table" class="table table-bordered table-striped" style="width:100%">
                      <thead>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Satuan</th>
                        <th>Tipe</th>
                        <th>Deskripsi</th>
                        <th>Harga Awal</th>
                        <th>Harga Akhir</th>
                        <th>Jumlah</th>
                        <th>Subtotal</th>
                      </thead>
                      <tbody id="salesproduct-list">
                      </tbody>
                    </table>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-12">
                    <label for="salespayment-table">List Pembayaran</label>
                    <table id="salespayment-table" class="table table-bordered table-striped" style="width:100%">
                      <thead>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nominal</th>
                        <th>Penerima</th>
                        <th>Pembayar</th>
                      </thead>
                      <tbody id="salespayment-list">
                        <tr>
                          <td>1</td>
                          <td>2018</td>
                          <td>1000</td>
                          <td>Harta</td>
                          <td>Derin</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div id="loadingSpinner" class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div><!-- /.box -->
          </div>
        </div>
      </div>
      <div class="modal fade" id="modal-add-payment">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Tambah barang baru</h4>
            </div>
            <div class="modal-body">
              <form id="pform" class="form-horizontal" role="form">
                <input id="sid" name="sid" type="hidden" value="<?php echo $sid; ?>">
                <div class="form-group">
                  <label for="amount" class="col-sm-2 control-label">Nominal</label>
                  <div class="col-sm-10">
                    <input name="amount" type="text" class="divide form-control" id="amount" placeholder="Nominal">
                  </div>
                </div>
                <div class="form-group">
                  <label for="courier" class="col-sm-2 control-label">Pembayar</label>
                  <div class="col-sm-10">
                    <input name="courier" type="text" class="form-control" id="courier" placeholder="Pembayar">
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
              <button id="add-paymentp" type="button" class="btn btn-primary">Tambah pembayaran</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php echo $footer ?>

  <!-- Control Sidebar -->
  <?php echo $sidebar_control ?>
</div>
<!-- ./wrapper -->

<?php echo $scripts; ?>

<script type="text/javascript">
  $(document).ready(function($) {

    $('#add-payment').click(function () {
      $('#modal-add-payment').modal();
    });

    let pform = $('#pform').validate({
      rules: {
        amount: {
          required : true
        },
        courier: {
          required: true
        }
      },errorPlacement: function(label, element) {
        label.addClass('help-block');
        label.insertAfter(element);
      },
      wrapper: 'span',
      submitHandler: function(form) {

        let url = "<?php echo base_url('Sales/addSalespaymentp'); ?>";
        let data = $('#pform').serialize();
        console.log(data);
        let success = (data)=>{
          
          if (data.status==1) {
            populateSalesPayment();
            populateSalesDetail();
            $('#modal-add-payment').modal('hide');
            alert("alert-success","Pembayaran berhasil disimpan");
            $("#pform")[0].reset();
            pform.resetForm();
          }
        }

        let error = (data)=>{
          alert('Unexpected error');
        }

        postRequest(url,data,success,error);
        return false;
      }
    });

    $('#add-paymentp').click(function () {
      $('#pform').submit();
    })

    populateSalesProduct();
    populateSalesDetail();
    populateSalesPayment();

    loading();

    function populateSalesProduct() {

      let sid = $('#sid').val();

      let url = "<?php echo base_url('Sales/getSalesproduct'); ?>";
      let data = {sid : sid};
      
      let success = (data)=>{
        console.log(data);
        $("#salesproduct-table").dataTable().fnDestroy();
        $('#salesproduct-list').empty();
        if (!data.message){
          $('#salesproduct-table').DataTable({
            responsive: true, 
            scrollX: true,
            paginate: true,
            "columnDefs": [
              {"orderable": false, "targets": [7] }
            ],
            "aaSorting": []
          });
          loading();
          return;
        }

        const numberWithCommas = (x) => {
          return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        for (var i = 0; i < data.message.product.length; i++) {
          let x = data.message.product[i];
          let type = x.item_type==1?'DISTRIBUTOR':'AGEN';
          let row = `
          <tr>
            <td>`+(i+1)+`</td>
            <td>`+x.item_name+`</td>
            <td>`+x.item_unit+`</td>
            <td>`+type+`</td>
            <td>`+x.item_desc+`</td>
            <td>Rp. `+numberWithCommas(x.item_dprice)+`</td>
            <td>Rp. `+numberWithCommas(x.item_fprice)+`</td>
            <td>`+x.item_qty+`</td>
            <td>Rp. `+numberWithCommas((x.item_fprice*x.item_qty))+`</td>
          </tr>
          `;

          $('#salesproduct-list').append(row);
        }

        $('#salesproduct-table').DataTable({
          responsive: true, 
          scrollX: true,
          paginate: true,
          "columnDefs": [
            {"orderable": false, "targets": [7] }
          ],
          "aaSorting": []
        });

        loading();
      }

      let error = (data)=>{
        console.log(data);
        alert('Unknown Error');
        loading();
      }

      loading();
      getRequest(url, data, success, error);
    }

    function populateSalesDetail() {
      let sid = $('#sid').val();

      let url = "<?php echo base_url('Sales/getSalesdetail'); ?>";
      let data = {sid : sid};
      
      let success = (data)=>{

        const numberWithCommas = (x) => {
          return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        let s = data.message.sales;

        $('#scode').val(s.code);
        $('#sdate').val(s.createdt);
        $('#scname').val(s.customer_name);
        $('#scphone').val(s.customer_phone);
        $('#scaddress').val(s.customer_address);
        $('#stotal').val('Rp. '+numberWithCommas(s.total));
        $('#spaid').val('Rp. '+numberWithCommas(s.customer_paid));
        $('#sdisc').val((s.disc*100)+'%');
        if(s.total>s.customer_paid){$('#btn-kredit').show();}
        else{$('#btn-kredit').hide();}

        loading();
      }

      let error = (data)=>{
        console.log(data);
        alert('Unknown Error');
        loading();
      }

      loading();
      getRequest(url, data, success, error);
    }

    function populateSalesPayment() {

      let sid = $('#sid').val();

      let url = "<?php echo base_url('Sales/getSalespayment'); ?>";
      let data = {sid : sid};

      let success = (data)=>{
        console.log(data);
        $("#salespayment-table").dataTable().fnDestroy();
        $('#salespayment-list').empty();
        if (!data.message){
          $('#salespayment-table').DataTable({
            responsive: true, 
            scrollX: true,
            paginate: true,
            "columnDefs": [
            ],
            "aaSorting": []
          });
          loading();
          return;
        }

        const numberWithCommas = (x) => {
          return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        for (var i = 0; i < data.message.payment.length; i++) {
          let x = data.message.payment[i];
          let row = `
          <tr>
            <td>`+(i+1)+`</td>
            <td>`+x.created+`</td>
            <td>`+numberWithCommas(x.amount)+`</td>
            <td>`+x.receiver+`</td>
            <td>`+x.courier+`</td>
          </tr>
          `;

          $('#salespayment-list').append(row);
        }

        $('#salespayment-table').DataTable({
          responsive: true, 
          scrollX: true,
          paginate: true,
          "columnDefs": [
          ],
          "aaSorting": []
        });

        loading();
      }

      let error = (data)=>{
        console.log(data);
        alert('Unknown Error');
        loading();
      }

      loading();
      getRequest(url, data, success, error);
    }

    function loading() {
      $('#loadingSpinner').toggle();
    }

    function getRequest(url, data, success, error) {
      $.ajax({
        url : url,
        data : data,
        type : 'GET',
        success : success,
        error : error
      });
    }

    function postRequest(url, data, success, error) {
      $.ajax({
        url : url,
        data : data,
        type : 'POST',
        success : success,
        error : error
      });
    }

  })
</script>
</body>
</html>