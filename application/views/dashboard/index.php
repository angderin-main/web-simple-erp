<!DOCTYPE html>
<html>
<head>
  <?php echo $head; ?>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
<div class="wrapper">

  <!-- Main Header -->
  <?php echo $header; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php echo $sidebar ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Page Header
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <div class="row">
        <div class="col-md-6" id="graph-omset-agent"></div>
        <div class="col-md-6" id="graph-omset-distributor"></div>
      </div>
      <!--------------------------
        | Your Page Content Here |
        -------------------------->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php echo $footer ?>

  <!-- Control Sidebar -->
  <?php echo $sidebar_control ?>
</div>
<!-- ./wrapper -->

<?php echo $scripts; ?>
<script src="https://code.highcharts.com/highcharts.src.js"></script>

<script>
  $(document).ready(function () {
    console.log("asdsad");
    Highcharts.chart('graph-omset-agent', {

      title: {
          text: 'Omset Penjualan Trading'
      },

      subtitle: {
          text: ''
      },
      xAxis: {
          categories: [
            '01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'
          ]
      },
      yAxis: {
          title: {
              text: 'IDR'
          },
          max: <?php if($totalomset2==0)echo 100000; else echo $totalomset2; ?>
      },
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
      },

      plotOptions: {
          series: {
              label: {
                  connectorAllowed: false
              }
          }
      },

      series: [{
          name: 'Agent',
          data: [
            <?php foreach($omset2 as $o){
                echo $o.",";
            } ?>
          ]
      }],

      responsive: {
          rules: [{
              condition: {
                  maxWidth: 500
              },
              chartOptions: {
                  legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                  }
              }
          }]
      }

      });

      Highcharts.chart('graph-omset-distributor', {

      title: {
          text: 'Omset Penjualan Distributor'
      },

      subtitle: {
          text: ''
      },
      xAxis: {
          categories: [
            '01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'
          ]
      },
      yAxis: {
          title: {
              text: 'IDR'
          },
          max: <?php if($totalomset1==0)echo 100000; else echo $totalomset2; ?>
      },
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
      },

      plotOptions: {
          series: {
              label: {
                  connectorAllowed: false
              }
          }
      },

      series: [{
          name: 'Distributor',
          data: [
              <?php foreach($omset1 as $o){
                  echo $o.",";
              } ?>
          ]
      }],

      responsive: {
          rules: [{
              condition: {
                  maxWidth: 500
              },
              chartOptions: {
                  legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                  }
              }
          }]
      }

      });
  });
</script>

</body>
</html>