<main>		

      <table>

        <thead>

          <tr>                     	

            <th colspan=6>

            	<div id="details" class="clearfix">

		        <div id="client">

		          <div class="to"><b>MERCHANDISE MUTASI</b></div>

		          <h2 class="name">Nomor : <?php echo $id?></h2>     

		          <h2 class="name">Tanggal : <?php echo date('d-m-Y',strtotime($courierdt))?></h2> 	                

		        </div>

		        <div id="invoice">          

		          <div class="date">From: <b><?php echo $fromname?></b></div> 

		          <div class="date">To: <b><?php echo $toname?></b></div>     

		          <div class="date">Note: <?php echo $note?></b></div>  		          

		        </div>

		    </div>

            </th>                            

          </tr>

          <tr> <th colspan=6 style="text-align:center; border: 1px solid #000000"><strong>DESCRIPTION</strong></th></tr>

          

        </thead>        

		<tbody class="body">		

			<tr>

			<?php 

			$x=0;

			$no=1;

			foreach ($products as $row){?>		

				<td class="desc">		

					<div class="row">

						<div class="thumb"><?php echo $no++?>) </div>

						<div class="thumb">

							<?php echo $row->code?>

						</div>

					</div>					

				</td>

				<td class="desc"><?php echo $row->name?></td>	

				<td class="desc"><?php echo $row->qty?></td>			

				<?php 				

				if (($x + 1) % 2 == 0){

					echo '</tr><tr>';

				}

				$x++;

			

			}?>

			</tr>			

		</tbody>

	</table>

	<div class="row">

		<div class="column">

			 <h3>Yang Membuat</h3>

			<div class="signature">

				<?php echo $createdbyname?>

			</div>

		</div>

		<div class="column">

			 <h3>Yang Membawa</h3>

			<div class="signature">

				<?php echo $couriername?>

			</div>

		</div>

		<div class="column">

			 <h3>Yang Menerima</h3>

			<div class="signature">

				<div>...</div>

			</div>

		</div>	

	</div>	 

	

</main>

