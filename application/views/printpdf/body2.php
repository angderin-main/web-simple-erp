<main>		
	<table>
		<thead>
			<!-- <tr>
				<th colspan=12 style="text-align: center; font-size: 18pt">INVOICE</th>
			</tr> -->
			<!-- <tr>
				<th colspan=12>
					<div id="details" class="clearfix">
						<div id="client">
							<div class="to"><b>INVOICE</b></div>
							<h2 class="name">Nomor : <?php echo $id?></h2>     
							<h2 class="name">Tanggal : <?php echo date('d-m-Y',strtotime($createdt))?></h2> 	                
						</div>
						<div id="invoice">          
							<div class="date">From: <b><?php echo $createdby?></b></div> 
							<div class="date">To: <b><?php echo $customer_name?></b></div>
						</div>
					</div>
				</th>                            
			</tr> -->
			<!-- <tr>
				<th colspan="6">Kode: <?php echo $code; ?></th>
				<th colspan="6">Kepada: <?php echo $customer_name; ?></th>
			</tr> -->
			<tr>
				<th style="text-align: left; border-top: black 1px dotted; border-bottom: black 1px dotted" colspan="1" >No</th>
				<th style="text-align: left; border-top: black 1px dotted; border-bottom: black 1px dotted" colspan="4" >Nama</th>
				<th style="text-align: center; border-top: black 1px dotted; border-bottom: black 1px dotted" colspan="1" >Satuan</th>
				<th style="text-align: center; border-top: black 1px dotted; border-bottom: black 1px dotted" colspan="2" >Jumlah Brg</th>
				<th style="text-align: center; border-top: black 1px dotted; border-bottom: black 1px dotted" colspan="1" >Hrg Satuan</th>
				<th style="text-align: center; border-top: black 1px dotted; border-bottom: black 1px dotted" colspan="4" >Jumlah Rp</th>
			</tr>
		</thead>
		<tbody class="body">
			<?php $subtotal=0; $i=$counter+1; foreach ($products as $key => $p) : ?>
			<tr>
				<td style="text-align: center;" colspan="1" ><?php echo $i; ?></td>
				<td style="text-align: left;" colspan="4" ><?php echo $p->item_name; ?></td>
				<td style="text-align: center;" colspan="1" ><?php echo $p->item_unit; ?></td>
				<td style="text-align: center;" colspan="2" ><?php echo $p->item_qty; ?></td>
				<?php if($type=='1'){ ?>
				<td style="text-align: center;" colspan="1" ><?php echo "".number_format($p->item_dprice,0,",","."); ?></td>
				<td style="text-align: center;" colspan="4" ><?php $subtotal = $p->item_dprice*$p->item_qty; echo "".number_format($subtotal,0,",","."); ?></td>
				<?php }else{ ?>
				<td style="text-align: center;" colspan="1" ><?php echo "".number_format($p->item_fprice,0,",","."); ?></td>
				<td style="text-align: center;" colspan="4" ><?php $subtotal = $p->item_fprice*$p->item_qty; echo "".number_format($subtotal,0,",","."); ?></td>
				<?php } ?>
			</tr>
			<?php $i++; endforeach; ?>
		</tbody>
	</table>
	<?php if($i == $size+1){ ?> 
		<?php if($type == '1'){ ?>
	<div style="position: absolute; right: 27px; bottom: 0px; padding: 0px; margin: 0px; top: 355px; text-align: right;">
		<b>Total : Rp. <?php echo number_format($dtotal,0,",","."); ?></b><br>
	</div>
	<div style="position: absolute; right: 27px; bottom: 0px; padding: 0px; margin: 0px; top: 365px; text-align: right;">
		<b>Grand Total (Disc <?php echo $disc*100; ?>%) : Rp. <?php echo number_format($total,0,",","."); ?></b><br>
	</div>
		<?php }else { ?>
	<div style="position: absolute; right: 27px; bottom: 0px; padding: 0px; margin: 0px; top: 355px; text-align: right;">
		<b>Total : Rp. <?php echo number_format($total,0,",","."); ?></b><br>
	</div>
		<?php } ?>
	<?php } ?>
</main>