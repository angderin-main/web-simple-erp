<!DOCTYPE html>
<html>
<head>
  <?php echo $head; ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <?php echo $header; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php echo $sidebar ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pelanggan
        <small>Harga khusus</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Pelanggan</a></li>
        <li class="active">Harga khusus</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-11">
          <div class="col-md-3">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Menu</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <button id="add-product" class="btn btn-primary btn-block" ><i class="fa fa-plus" aria-hidden="true"></i> Tambah pelanggan</button>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
          <div class="col-md-9">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Tabel Pelanggan</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <table id="customer-table" class="table table-bordered table-striped" style="width:100%">
                  <thead>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Telepon</th>
                    <th>Alamat</th>
                    <th>Menu</th>
                  </thead>
                  <tbody id="customer-list">
                  </tbody>
                </table>
              </div><!-- /.box-body -->
              <div id="loadingSpinner" class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div><!-- /.box -->
          </div>
        </div>
      </div>
      <div class="modal fade" id="modal-add-customer">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Tambah pelanggan baru</h4>
            </div>
            <div class="modal-body">
              <form id="cform" class="form-horizontal" role="form">
                <div class="form-group">
                  <label for="pname" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input name="cname" type="text" class="form-control" id="cname" placeholder="Nama pelanggan">
                  </div>
                </div>
                <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Telepon</label>
                  <div class="col-sm-10">
                    <input name="cphone" type="text" class="form-control" id="cphone" placeholder="Telepon">
                  </div>
                </div>
                <div class="form-group">
                  <label for="punit" class="col-sm-2 control-label">Alamat</label>
                  <div class="col-sm-10">
                    <textarea name="caddress" type="text" class="form-control" id="caddress" placeholder="Alamat"></textarea>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
              <button id="add-customerp" type="button" class="btn btn-primary">Tambah pelanggan</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <div class="modal fade" id="modal-edit-customer">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Edit pelanggan</h4>
            </div>
            <div class="modal-body">
              <form id="editform" class="form-horizontal" role="form">
                <input name="cid" type="hidden" id="cid">
                <div class="form-group">
                  <label for="pname" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input name="cname" type="text" class="form-control" id="cname" placeholder="Nama pelanggan">
                  </div>
                </div>
                <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Telepon</label>
                  <div class="col-sm-10">
                    <input name="cphone" type="text" class="form-control" id="cphone" placeholder="Telepon">
                  </div>
                </div>
                <div class="form-group">
                  <label for="punit" class="col-sm-2 control-label">Alamat</label>
                  <div class="col-sm-10">
                    <textarea name="caddress" type="text" class="form-control" id="caddress" placeholder="Alamat"></textarea>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
              <button id="delete-customerp" type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
              <button id="edit-customerp" type="button" class="btn btn-primary">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php echo $footer ?>

  <!-- Control Sidebar -->
  <?php echo $sidebar_control ?>
</div>
<!-- ./wrapper -->

<?php echo $scripts; ?>

<script type="text/javascript">
  $(document).ready(function () {

    let cform = $('#cform').validate({
      rules: {
        cname: {
          required : true
        }
      },errorPlacement: function(label, element) {
        label.addClass('help-block');
        label.insertAfter(element);
      },
      wrapper: 'span',
      submitHandler: function(form) {

        let url = "<?php echo base_url('Customer/addcustomerp'); ?>";
        let data = $('#cform').serialize();

        let success = (data)=>{
          if (data.status==1) {
            populateclist();
            $('#modal-add-customer').modal('hide');
            $("#cform")[0].reset();
            cform.resetForm();
          }
        }

        let error = (data)=>{
          alert('Unexpected error');
        }

        postRequest(url,data,success,error);
        return false;
      }
    });

    let editform = $('#editform').validate({
      rules: {
        cname: {
          required : true
        },
        cphone: {
          required: true
        }
      },errorPlacement: function(label, element) {
        label.addClass('help-block');
        label.insertAfter(element);
      },
      wrapper: 'span',
      submitHandler: function(form) {

        let url = "<?php echo base_url('Customer/editcustomerp'); ?>";
        let data = $('#editform').serialize();

        let success = (data)=>{
          if (data.status==1) {
            populateclist();
            $('#modal-edit-customer').modal('hide');
            $("#editform")[0].reset();
            cform.resetForm();
          }
        }

        let error = (data)=>{
          alert('Unexpected error');
        }

        postRequest(url,data,success,error);
        return false;
      }
    });

    $('#add-product').click(function () {
      $('#modal-add-customer').modal();
    });

    $('#add-customerp').click(function () {
      $('#cform').submit();
    });

    $('#edit-customerp').click(function () {
      $('#editform').submit();
    });

    $('#delete-customerp').click(function () {
      if (confirm('Yakin hapus produk?')) {
        let id_item = $('#cid').val();

        let url = "<?php echo base_url('Customer/deletecustomerp'); ?>";
        let data = {
          id : id_item
        };

        let success = (data)=>{
          if (data.status==1) {
            populateclist();
            $('#modal-edit-customer').modal('hide');
          }
        }

        let error = (data)=>{
          alert('Unexpected error');
        }

        postRequest(url,data,success,error);
      }
    });

    populateclist();

    loading();

    function populateclist() {

      let base_url = "<?php echo base_url(); ?>";
      let url = "<?php echo base_url('Customer/getcustomer'); ?>";
      let data = {};
      
      let success = (data)=>{
        console.log(data);
        $("#customer-table").dataTable().fnDestroy();
        $('#customer-list').empty();
        if (!data.message){
          $('#customer-table').DataTable({
            responsive: true,
            paginate: true,
            "columnDefs": [
              {"orderable": false, "targets": [0] }
            ],
            "aaSorting": []
          });
          loading();
          return;
        }

        for (var i = 0; i < data.message.length; i++) {
          let x = data.message[i];
          let row = `
          <tr>
            <td>`+(i+1)+`</td>
            <td>`+x.name+`</td>
            <td>`+x.phone+`</td>
            <td>`+x.address+`</td>
            <td>
              <button id=`+x.id+` type="button" class="btn btn-primary c-sprice">Harga Khusus</button>
              <button cid=`+x.id+` cname="`+x.name+`" cphone="`+x.phone+`" caddress="`+x.address+`" type="button" class="btn btn-primary edit-customer"><i class="fa fa-edit" aria-hidden="true"></i></button>
              <!-- <button id=`+x.id+` type="button" class="btn btn-danger btn-block c-delete">Hapus</button> -->
            </td>
          </tr>
          `;

          $('#customer-list').append(row);
        }

        // $('.c-delete').click(function () {
        //   if (confirm('Yakin hapus pelanggan?')) {
        //     let id = $(this).attr('id');

        //     let url = "<?php echo base_url('Customer/deletecustomerp'); ?>";
        //     let data = {
        //       id : id
        //     };

        //     let success = (data)=>{
        //       if (data.status==1) {
        //         populateclist();
        //       }
        //     }

        //     let error = (data)=>{
        //       alert('Unexpected error');
        //     }

        //     postRequest(url,data,success,error);
        //   }
        // });

        $('.edit-customer').click(function () {
          let modal = $('#modal-edit-customer');
          let c = $(this);
          modal.find('#cid').val(c.attr('cid'));
          modal.find('#cname').val(c.attr('cname'));
          modal.find('#caddress').val(c.attr('caddress'));
          modal.find('#cphone').val(c.attr('cphone'));

          $('#modal-edit-customer').modal();
        });

        $('.c-sprice').click(function () {
          let id = $(this).attr('id');
          let url = base_url+"customer-price?cid="+id;
          window.location.replace(url);
        });

        $('#customer-table').DataTable({
          responsive: true, 
          paginate: true,
          "columnDefs": [
            {"orderable": false, "targets": [0] }
          ],
          "aaSorting": []
        });

        loading();
      }

      let error = (data)=>{
        console.log(data);
        alert('Unknown Error');
        loading();
      }

      loading();
      getRequest(url, data, success, error);
    }

    function loading() {
      $('#loadingSpinner').toggle();
    }

    function getRequest(url, data, success, error) {
      $.ajax({
        url : url,
        data : data,
        type : 'GET',
        success : success,
        error : error
      });
    }

    function postRequest(url, data, success, error) {
      $.ajax({
        url : url,
        data : data,
        type : 'POST',
        success : success,
        error : error
      });
    }

  })
</script>

</body>
</html>