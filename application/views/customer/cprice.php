<!DOCTYPE html>
<html>
<head>
  <?php echo $head; ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <?php echo $header; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php echo $sidebar ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pelanggan
        <small>Harga khusus</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('customer'); ?>"><i class="fa fa-dashboard"></i> Pelanggan</a></li>
        <li class="active">Harga khusus</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-11">
          <div class="col-md-3">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Menu</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <button id="add-cprice" class="btn btn-primary btn-block" > Harga khusus</button>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
          <div class="col-md-9">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Harga khusus untuk <b><?php echo $cname; ?></b></h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <table id="cprice-table" class="table table-bordered table-striped" style="width:100%">
                  <thead>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Harga standart</th>
                    <th>Harga khusus</th>
                    <th>Satuan</th>
                    <th>Tipe</th>
                    <th>Deskripsi</th>
                    <th>Menu</th>
                  </thead>
                  <tbody id="cprice-list">
                    <!-- <tr>
                      <td>1</td>
                      <td>Lorem</td>
                      <td>123123</td>
                      <td>32131231</td>
                      <td>ASD</td>
                      <td>Type</td>
                      <td>DESKRIP</td>
                      <td>WKWKWKW</td>
                    </tr> -->
                  </tbody>
                </table>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
        </div>
      </div>
      <div class="modal fade" id="modal-add-cprice">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Tambah harga khusus untuk <b><?php echo $cname ?></b></h4>
            </div>
            <div class="modal-body">
              <form id="cpform" class="form-horizontal" role="form">
                <div class="form-group">
                  <label for="pname" class="col-sm-2 control-label">Barang</label>
                  <div class="col-sm-10">
                    <select name="pid" id="pid" class="form-control select2" style="width: 100%;">
                      <option selected="selected">Pilih barang</option>
                    </select>
                  </div>
                </div>
                <input type="hidden" id="cid" name="cid" readonly="" value="<?php echo $cid; ?>">
                <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="pname" placeholder="Nama barang" disabled="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Harga</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="pprice" placeholder="Harga standart" disabled="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Satuan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="punit" placeholder="Satuan barang" disabled="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Tipe</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="ptype" placeholder="Tipe barang" disabled="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Deskripsi</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" id="pdesc" placeholder="Deskripsi barang" disabled=""></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Harga khusus</label>
                  <div class="col-sm-10">
                    <input name="cprice" type="text" class="form-control" id="cprice" placeholder="Harga khusus">
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
              <button id="add-cpricep" type="button" class="btn btn-primary">Tambah harga khusus</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <div class="modal fade" id="modal-edit-cprice">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Ubah harga khusus untuk <b><?php echo $cname ?></b></h4>
            </div>
            <div class="modal-body">
              <form id="edit-cpform" class="form-horizontal" role="form">
                <input type="hidden" id="cpid" name="cpid" readonly="" value="">
                <input type="hidden" id="pid" name="pid" readonly="" value="">
                <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="pname" placeholder="Nama barang" disabled="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Harga</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="pprice" placeholder="Harga standart" disabled="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Satuan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="punit" placeholder="Satuan barang" disabled="">
                  </div>
                </div>
                <!-- <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Tipe</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="ptype" placeholder="Tipe barang" disabled="">
                  </div>
                </div> -->
                <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Deskripsi</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" id="pdesc" placeholder="Deskripsi barang" disabled=""></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Harga khusus</label>
                  <div class="col-sm-10">
                    <input name="cprice" type="text" class="form-control" id="cprice" placeholder="Harga khusus">
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
              <button type="button" class="btn btn-danger cp-delete">Hapus</button>
              <button id="edit-cpricep" type="submit" class="btn btn-primary">Simpan harga khusus</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php echo $footer ?>

  <!-- Control Sidebar -->
  <?php echo $sidebar_control ?>
</div>
<!-- ./wrapper -->

<?php echo $scripts; ?>
<script type="text/javascript">
$(document).ready(function ($) {
    $('.select2').select2();

    let cpform = $('#cpform').validate({
      rules: {
        pid: {required: true},
        cid: {required: true},
        cprice: {required: true}
      },errorPlacement: function(label, element) {
        label.addClass('help-block');
        label.insertAfter(element);
      },
      wrapper: 'span',
      submitHandler: function(form) {

        let url = "<?php echo base_url('Customer/addcpricep'); ?>";
        let data = $('#cpform').serialize();

        console.log(data);

        console.log(data);

        let success = (data)=>{
          if (data.status==1) {
            populatecplist();
            $('#modal-add-cprice').modal('hide');
            $("#cpform")[0].reset();
            cpform.resetForm();
          }
        }

        let error = (data)=>{
          alert('Unexpected error');
        }

        postRequest(url,data,success,error);
        return false;
      }
    });

    let editcpform = $('#edit-cpform').validate({
      rules: {
        // pid: {required: true},
        // cpid: {required: true},
        // cprice: {required: true}
      },errorPlacement: function(label, element) {
        label.addClass('help-block');
        label.insertAfter(element);
      },
      wrapper: 'span',
      submitHandler: function(form) {

        let url = "<?php echo base_url('Customer/editcpricep'); ?>";
        let data = $('#edit-cpform').serialize();

        console.log(data);

        let success = (data)=>{
          if (data.status==1) {
            populatecplist();
            $('#modal-edit-cprice').modal('hide');
            $("#edit-cpform")[0].reset();
            cpform.resetForm();
          }
        }

        let error = (data)=>{
          alert('Unexpected error');
        }

        postRequest(url,data,success,error);
        return false;
      }
    });

    $('#add-cprice').click(function () {
      let url = "<?php echo base_url('Customer/getproduct'); ?>";
      let data = "";

      let success = (data)=>{
        if (data.status==1) {
          $('#pid').empty();
          let row = `
          <option selected="selected">Pilih barang</option>
          `;
          $('#pid').append(row);
          for (var i = 0; i < data.message.length; i++) {
            let x = data.message[i];
            let row = `
            <option value="`+x.id+`" pname="`+x.name+`" pprice="`+x.defaultp+`" punit=`+x.unit+` ptype="`+x.type+`" pdesc="`+x.desc+`">`+x.name+`</option>
            `;
            $('#pid').append(row);
          }
        }
      }

      let error = (data)=>{
        alert('Unexpected error');
      }

      postRequest(url,data,success,error);

      $('#modal-add-cprice').modal();
    });

    $('#pid').change(function () {
      let product = $('#pid').find(":selected");
      $('#pname').val(product.attr('pname'));
      $('#pprice').val(product.attr('pprice'));
      $('#punit').val(product.attr('punit'));
      $('#ptype').val(product.attr('ptype'));
      $('#pdesc').val(product.attr('pdesc'));
    });

    $('#add-cpricep').click(function () {
      $('#cpform').submit();
    });

    $('#edit-cpricep').click(function () {
      $('#edit-cpform').submit();
    });

    populatecplist();

    loading();

    function populatecplist() {

      let base_url = "<?php echo base_url(); ?>";
      let url = "<?php echo base_url('Customer/getcprice'); ?>";
      let data = {
        cid: <?php echo $cid ?>
      };
      
      let success = (data)=>{
        console.log(data);
        $("#cprice-table").dataTable().fnDestroy();
        $('#cprice-list').empty();
        if (!data.message){
          $('#cprice-table').DataTable({
            responsive: true,
            paginate: true,
            "columnDefs": [
              {"orderable": false, "targets": [0] }
            ],
            "aaSorting": []
          });
          loading();
          return;
        }

        for (var i = 0; i < data.message.length; i++) {
          let x = data.message[i];
          let row = `
          <tr>
            <td>`+(i+1)+`</td>
            <td>`+x.name+`</td>
            <td>`+x.defaultp+`</td>
            <td>`+x.custom_price+`</td>
            <td>`+x.unit+`</td>
            <td>`+x.type+`</td>
            <td>`+x.desc+`</td>
            <td>
              <button cpid=`+x.cpid+` pid=`+x.pid+` cpname='`+x.name+`' cpdefaultp=`+x.defaultp+` cpcustom=`+x.custom_price+` cpunit=`+x.unit+` cpdesc='`+x.desc+`' type="button" class="btn btn-primary cp-edit">Edit</button>
              <!--<button id=`+x.id+` type="button" class="btn btn-danger cp-delete">Hapus</button>-->
            </td>
          </tr>
          `;

          $('#cprice-list').append(row);
        }

        $('.cp-delete').click(function () {
          console.log($('#modal-edit-cprice').find('#cpid').val());
          if (confirm('Yakin hapus pelanggan?')) {
            let id = $('#modal-edit-cprice').find('#cpid').val();

            let url = "<?php echo base_url('Customer/deletecpricep'); ?>";
            let data = {
              id : id
            };

            let success = (data)=>{
              if (data.status==1) {
                $('#modal-edit-cprice').modal('hide');
                populateclist();
              }
            }

            let error = (data)=>{
              alert('Unexpected error');
            }

            postRequest(url,data,success,error);
          }
        });

        $('.cp-edit').click(function () {
          let cpid = $(this).attr('cpid');
          let pid = $(this).attr('pid');
          let name = $(this).attr('cpname');
          let defaultp = $(this).attr('cpdefaultp');
          let customp = $(this).attr('cpcustom');
          let unit = $(this).attr('cpunit');
          let desc = $(this).attr('cpdesc');

          let modal = $('#modal-edit-cprice');

          modal.find('#cpid').val(cpid);
          modal.find('#pid').val(pid);
          modal.find('#pname').val(name);
          modal.find('#pprice').val(defaultp);
          modal.find('#punit').val(unit);
          modal.find('#pname').val(name);
          modal.find('#pdesc').val(desc);
          modal.find('#cprice').val(customp);

          modal.modal('show');

        });

        $('#cprice-table').DataTable({
          responsive: true,
          paginate: true,
          "columnDefs": [
            {"orderable": false, "targets": [0] }
          ],
          "aaSorting": []
        });

        loading();
      }

      let error = (data)=>{
        console.log(data);
        alert('Unknown Error');
        loading();
      }

      loading();
      getRequest(url, data, success, error);
    }

    function loading() {
      $('#loadingSpinner').toggle();
    }

    function getRequest(url, data, success, error) {
      $.ajax({
        url : url,
        data : data,
        type : 'GET',
        success : success,
        error : error
      });
    }

    function postRequest(url, data, success, error) {
      $.ajax({
        url : url,
        data : data,
        type : 'POST',
        success : success,
        error : error
      });
    }
})
</script>
</body>
</html>