  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="https://placehold.it/160x160" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('user')['fullname']; ?></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form class="sidebar-form" onkeypress="return event.keyCode != 13;">
        <div class="input-group">
          <input type="text" name="sidebar-search" id="sidebar-search" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="button" name="btn-sidebar-search" id="btn-sidebar-search" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" id="sidebar-menu" data-widget="tree">
        <li class="header gudang">GUDANG</li>
        <li class="gudang-barang"><a href="<?php echo base_url("product") ?>"><i class="fa fa-table"></i> <span>Barang</span></a></li>
        <li class="gudang-stok"><a href="<?php echo base_url("stock") ?>"><i class="fa fa-table"></i> <span>Stok</span></a></li>
        <li class="gudang-supplier"><a href="<?php echo base_url("supplier") ?>"><i class="fa fa-table"></i> <span>Supplier</span></a></li>
        <li class="header pelanggan">PELANGGAN</li>
        <li class="pelanggan-pelanggan"><a href="<?php echo base_url("customer") ?>"><i class="fa fa-table"></i> <span>Pelanggan</span></a></li>
        <!-- <li><a href="<?php echo base_url("custom") ?>"><i class="fa fa-table"></i> <span>Harga khusus</span></a></li> -->
        <!-- <li class="header">ADMINISTRATOR</li> -->
        <!-- <li><a href="<?php echo base_url("product") ?>"><i class="fa fa-table"></i> <span>User</span></a></li> -->
        <li class="header penjualan">PENJUALAN</li>
        <li class="penjualan-list penjualan"><a href="<?php echo base_url("history") ?>"><i class="fa fa-table"></i> <span>List Penjualan</span></a></li>
        <li class="penjualan-list harga"><a href="<?php echo base_url("pricelist") ?>"><i class="fa fa-table"></i> <span>List Harga</span></a></li>
        <li class="penjualan-kasir"><a href="<?php echo base_url("cashier") ?>"><i class="fa fa-table"></i> <span>Kasir</span></a></li>
        <!-- <li class="header">HEADER</li> -->
        <!-- Optionally, you can add icons to the links -->
        <!-- <li><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
        <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li> -->
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>