<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/adminlte/'); ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/adminlte/'); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url('assets/adminlte/'); ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/adminlte/'); ?>dist/js/adminlte.min.js"></script>

<script type="text/javascript" src="<?php echo base_url('assets/scripts/'); ?>jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/scripts/'); ?>dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/scripts/'); ?>jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/scripts/'); ?>additional-methods.min.js"></script>
<!-- number divider -->
<script src="<?php echo base_url('assets/scripts/'); ?>number-divider.min.js"></script>
<!-- My JS -->
<script src="<?php echo base_url('assets/scripts/'); ?>myjs.js" type="text/javascript"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->