<!DOCTYPE html>
<html>
<head>
  <?php echo $head; ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <?php echo $header; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php echo $sidebar ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gudang
        <small>Barang</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i>Gudang</a></li>
        <li class="active">Stock</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-2">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Menu</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <a href="<?php echo base_url('addstock') ?>"><button id="add-stock" class="btn btn-primary btn-block" ><i class="fa fa-plus" aria-hidden="true"></i> Tambah Stok</button></a>
                <br>
                <a href="<?php echo base_url('purchasestock') ?>"><button id="add-stock" class="btn btn-primary btn-block" ><i class="fa fa-plus" aria-hidden="true"></i> Catatan Stok</button></a>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
          <div class="col-md-10">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Tabel stok</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <table id="product-table" class="table table-bordered table-striped" style="width:100%">
                  <thead>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Satuan</th>
                    <th>Tipe</th>
                    <th>Deskripsi</th>
                    <th>Stock</th>
                    <th>Menu</th>
                  </thead>
                  <tbody id="product-list">
                  </tbody>
                </table>
              </div><!-- /.box-body -->
              <div id="loadingSpinner" class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div><!-- /.box -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php echo $footer ?>

  <!-- Control Sidebar -->
  <?php echo $sidebar_control ?>
</div>
<!-- ./wrapper -->

<?php echo $scripts; ?>

<script type="text/javascript">
  $(document).ready(function($) {

    populateItemList();

    loading();

    function populateItemList() {

      let url = "<?php echo base_url('Inventory/getstockp'); ?>";
      let data = {};
      
      let success = (data)=>{
        console.log(data);
        $("#product-table").dataTable().fnDestroy();
        $('#product-list').empty();
        if (!data.message){
          $('#product-table').DataTable({
            responsive: true,
            paginate: true,
            "columnDefs": [
              {"orderable": false, "targets": [0] }
            ],
            "aaSorting": []
          });
          loading();
          return;
        }

        for (var i = 0; i < data.message.length; i++) {
          let x = data.message[i];
          let type = x.type==1?'DISTRIBUTOR':'AGEN';
          let row = `
          <tr>
            <td>`+(i+1)+`</td>
            <td>`+x.name+`</td>
            <td>`+x.unit+`</td>
            <td>`+type+`</td>            
            <td>`+x.desc+`</td>
            <td>`+x.stock+`</td>
            <td>
              
            </td>
          </tr>
          `;

          $('#product-list').append(row);
        }

        $('#product-table').DataTable({
          responsive: true, 
          paginate: true,
          "columnDefs": [
            {"orderable": false, "targets": [0] }
          ],
          "aaSorting": []
        });

        loading();
      }

      let error = (data)=>{
        console.log(data);
        alert('Unknown Error');
        loading();
      }

      loading();
      getRequest(url, data, success, error);
    }

    function loading() {
      $('#loadingSpinner').toggle();
    }

  })
</script>
</body>
</html>