<!DOCTYPE html>
<html>
<head>
  <?php echo $head; ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <?php echo $header; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php echo $sidebar ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gudang
        <small>Tambah Stock</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i>Gudang</a></li>
        <li class="active">Tambah Stock</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="col-md-12">
        <form action="<?php echo base_url('Inventory/addstockp') ?>" method="POST">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Supplier</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="spselect" class="control-label">Supplier</label>
                      <select name="spid" id="spselect" class="form-control select2">
                        <option value="0" selected>Pilih supplier</option>
                        <?php foreach ($supplier as $sp) :?>
                        <option value="<?php echo $sp->id ?>" spname="<?php echo $sp->name ?>" spphone="<?php echo $sp->phone ?>" spaddress="<?php echo $sp->address ?>"><?php echo $sp->name ?></option>
                        <?php endforeach ?>
                      </select>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="spname" class="control-label">Nama</label>
                      <input name="spname" type="text" class="form-control" id="spname" placeholder="Nama supplier" value="Guest">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="spphone" class="control-label">Telepon</label>
                      <input name="spphone" type="text" class="form-control" id="spphone" placeholder="Telepon" value="0812345678">
                    </div>  
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="spaddress" class="control-label">Alamat</label>
                      <input name="spaddress" type="text" class="form-control" id="spaddress" placeholder="Alamat" value="Guest address">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <button class="btn btn-primary btn-block" type="button" id="btn-locksp" >Kunci supplier</button>
                    <button class="btn btn-warning btn-block" disabled="disabled" type="button" id="btn-resetsp">Reset supplier</button>
                  </div>
                </div>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Tabel stok</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                  <button type="button" class="btn btn-primary" id="btn-add" disabled="true"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Produk</button>
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <table id="stock-table" class="table table-bordered table-striped" style="width:100%">
                  <thead>
                    <th class="col-md-2">Produk</th>
                    <th class="col-md-1">Satuan</th>
                    <th class="col-md-1">Tipe</th>
                    <!-- <th>Deskripsi</th> -->
                    <th class="col-md-1">Stok Lama</th>
                    <th class="col-md-1">Stok Masuk</th>
                    <th class="col-md-3">Subtotal</th>
                    <th class="col-md-1">&nbsp;</th>
                  </thead>
                  <tbody id="stock-list">
                    <!-- <tr>
                      <td>
                        <select name="pid" id="pid" class="form-control select2" style="width:100%">
                          <option value="">Pilih Produk</option>
                          <option value="1">Kertas</option>
                        </select>
                      </td>
                      <td><input class="form-control" type="text" readonly value="Lembar"></td>
                      <td><input class="form-control" type="text" readonly value="Agen"></td>
                      <td><input class="form-control" type="text" readonly value="Kertas HVS A4"></td>
                      <td><input class="form-control" type="number" readonly value="10"></td>
                      <td><input class="form-control" type="number" name="qty" min="0" required></td>
                    </tr> -->
                  </tbody>
                </table>
              </div><!-- /.box-body -->
              <div id="stock-loading" class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div><!-- /.box -->
          </div>
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Catatan</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <textarea name="note" id="note" cols="30" rows="10" class="form-control" disabled='true'></textarea>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Simpan</button>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
        </form>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php echo $footer ?>

  <!-- Control Sidebar -->
  <?php echo $sidebar_control ?>
</div>
<!-- ./wrapper -->

<?php echo $scripts; ?>

<script type="text/javascript">
  $(document).ready(function($) {
    $('.select2').select2();
    loading('stock-loading');

    $('#btn-add').click(function () {
      addrow();
    });

    $('#spselect').change(function () {
      let supplier = $('#spselect').find(":selected");
      if (supplier.val()!=0) {
        $('#spname').val(supplier.attr('spname'));
        $('#spphone').val(supplier.attr('spphone'));
        $('#spaddress').val(supplier.attr('spaddress'));
        locksp();
      } else {
        resetsp();
      }
    });

    $('#btn-locksp').click(function(){
      locksp();
    });

    $('#btn-resetsp').click(function(){
      resetsp();
    });

    function addrow() {
      let row = `
      <tr>
        <td>
          <select name="pid[]" id="pid" class="form-control select2 pselect" style="width:100%">
            <option value="">Pilih Produk</option>
            <?php foreach($stock AS $s): ?>
            <option value="<?php echo $s->id; ?>" punit="<?php echo $s->unit; ?>" ptype="<?php echo $s->type; ?>" pdesc="<?php echo $s->desc; ?>" pstock=<?php echo $s->stock; ?>><?php echo $s->name; ?></option>
            <?php endforeach; ?>
          </select>
        </td>
        <td><input id="punit" class="form-control" type="text" readonly value=""></td>
        <td><input id="ptype" class="form-control" type="text" readonly value=""></td>
        <!--<td><input id="pdesc" class="form-control" type="text" readonly value=""></td>-->
        <td><input id="pstock" class="form-control" type="number" readonly value=""></td>
        <td><input class="form-control" type="number" name="qty[]" min="0" value="0"></td>
        <td><input class="divide form-control" type="text" name="subtotal[]" min="0" value="0"></td>
        <td class="text-center"><button type="button" class="btn btn-warning btn-remove" id="btn-add"><i class="fa fa-times" aria-hidden="true" required></i></button></td>
      </tr>
      `;

      $('#stock-list').append(row);
      $('.select2').select2();
      $('.pselect').change(function () {
        let product = $(this).find(":selected");
        let curr = $(this).parents('tr');
        let type = product.attr('ptype')==1?'DISTRIBUTOR':'AGEN';
        if (product.val()!="") {
          curr.find('#punit').val(product.attr('punit'));
          curr.find('#ptype').val(type);
          // curr.find('#pdesc').val(product.attr('pdesc'));
          curr.find('#pstock').val(product.attr('pstock'));
        } else {
          curr.find('#punit').val();
          curr.find('#ptype').val();
          curr.find('#pdesc').val();
          curr.find('#pstock').val();
        }
      });

      $('.btn-remove').click(function () {
        let curr = $(this).parents('tr').remove();
      })
    }

    function locksp() {
      $('#spselect').prop('disabled',true);
      $('#spname, #spphone, #spaddress').prop('readonly', true);
      $('#btn-resetsp').prop('disabled',false);
      $('#btn-locksp').prop('disabled',true);
      $('#btn-add').prop('disabled',false);
      $('#note').prop('disabled',false);
    }

    function resetsp() {
      $('#spselect').prop('disabled',false);
      $('#spname, #spphone, #spaddress').prop('readonly', false);
      $('#btn-resetsp').prop('disabled',true);
      $('#btn-locksp').prop('disabled',false);
      $('#spname, #spphone, #spaddress').val('');
      $("#spselect").prop('selectedIndex',0);
      $('#spselect').select2();
      $('#btn-add').prop('disabled',true);
      $('#stock-list').empty();
      $('#note').prop('disabled',true);
      $('#note').val();
    }
  })
</script>
</body>
</html>