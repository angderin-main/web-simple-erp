<!DOCTYPE html>
<html>
<head>
  <?php echo $head; ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <?php echo $header; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php echo $sidebar ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gudang
        <small>Barang</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i>Gudang</a></li>
        <li class="active">Barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-2">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Menu</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <button id="add-product" class="btn btn-primary btn-block" ><i class="fa fa-plus" aria-hidden="true"></i> Tambah produk</button>
                <!-- <br>
                <a href="<?php echo base_url('addstock') ?>"><button id="add-stock" class="btn btn-primary btn-block" ><i class="fa fa-plus" aria-hidden="true"></i> Tambah stock</button></a> -->
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
          <div class="col-md-10">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Tabel barang</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <table id="product-table" class="table table-bordered table-striped" style="width:100%">
                  <thead>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Satuan</th>
                    <th>Tipe</th>
                    <th>Deskripsi</th>
                    <th class="col-md-1">Menu</th>
                  </thead>
                  <tbody id="product-list">
                  </tbody>
                </table>
              </div><!-- /.box-body -->
              <div id="loadingSpinner" class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div><!-- /.box -->
          </div>
        </div>
      </div>
      <div class="modal fade" id="modal-add-product">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Tambah barang baru</h4>
            </div>
            <div class="modal-body">
              <form id="pform" class="form-horizontal" role="form">
                <div class="form-group">
                  <label for="pname" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input name="pname" type="text" class="form-control" id="pname" placeholder="Nama produk">
                  </div>
                </div>
                <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Harga</label>
                  <div class="col-sm-10">
                    <input name="pprice" type="number" class="form-control" id="pprice" placeholder="Harga">
                  </div>
                </div>
                <div class="form-group">
                  <label for="punit" class="col-sm-2 control-label">Satuan</label>
                  <div class="col-sm-10">
                    <input name="punit" type="text" class="form-control" id="punit" placeholder="Satuan">
                  </div>
                </div>
                <div class="form-group">
                  <label for="ptype" class="col-sm-2 control-label">Tipe</label>
                  <div class="col-md-10">
                    <select name="ptype" id="ptype" class="form-control">
                      <option value="1">DISTRIBUTOR</option>
                      <option value="2">AGEN</option>
                    </select>
                  </div>
                  <!-- <div class="col-sm-10">
                    <input name="ptype" type="text" class="form-control" id="ptype" placeholder="Tipe">
                  </div> -->
                </div>
                <div class="form-group">
                  <label for="pdesc" class="col-sm-2 control-label">Deskripsi</label>
                  <div class="col-sm-10">
                    <textarea name="pdesc" class="form-control" id="pdesc" placeholder="Deskripsi produk" ></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="pstock" class="col-sm-2 control-label">Stock</label>
                  <div class="col-sm-10">
                    <input name="pstock" type="number" class="form-control" id="psotck" placeholder="Satuan" value="0" min="0">
                  </div>
                </div>
                <hr>
                <h4>Tabel konversi</h4>
                <button id="add-product-convert" type="button" class="btn btn-primary pull-right" ><i class="fa fa-plus" aria-hidden="true"></i></button>
                <table id="convert-table" class="table table-striped">
                  <thead>
                    <!-- <th class="col-md-4">Item 1</th> -->
                    <th class="col-md-2">Qty 1</th>
                    <th class="col-md-4">Item 2</th>
                    <th class="col-md-2">Qty 2</th>
                    <th>&nbsp;</th>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
              <button id="add-productp" type="button" class="btn btn-primary">Tambah produk</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <div class="modal fade" id="modal-edit-product">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Edit barang</h4>
            </div>
            <div class="modal-body">
              <form id="editform" class="form-horizontal" role="form">
                <input name="pid" type="hidden" id="pid">
                <div class="form-group">
                  <label for="pname" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input name="pname" type="text" class="form-control" id="pname" placeholder="Nama produk">
                  </div>
                </div>
                <div class="form-group">
                  <label for="pprice" class="col-sm-2 control-label">Harga</label>
                  <div class="col-sm-10">
                    <input name="pprice" type="number" class="form-control" id="pprice" placeholder="Harga">
                  </div>
                </div>
                <div class="form-group">
                  <label for="punit" class="col-sm-2 control-label">Satuan</label>
                  <div class="col-sm-10">
                    <input name="punit" type="text" class="form-control" id="punit" placeholder="Satuan">
                  </div>
                </div>
                <div class="form-group">
                  <label for="ptype" class="col-sm-2 control-label">Tipe</label>
                  <div class="col-md-10">
                    <select name="ptype" id="ptype" class="form-control">
                      <option value="1">DISTRIBUTOR</option>
                      <option value="2">AGEN</option>
                    </select>
                  </div>
                  <!-- <div class="col-sm-10">
                    <input name="ptype" type="text" class="form-control" id="ptype" placeholder="Tipe">
                  </div> -->
                </div>
                <div class="form-group">
                  <label for="pdesc" class="col-sm-2 control-label">Deskripsi</label>
                  <div class="col-sm-10">
                    <textarea name="pdesc" class="form-control" id="pdesc" placeholder="Deskripsi produk" ></textarea>
                  </div>
                </div>
                <hr>
                <!-- <h4>Tabel konversi</h4>
                <button type="button" class="btn btn-primary pull-right" ><i class="fa fa-plus" aria-hidden="true"></i></button>
                <table id="convert-table" class="table table-striped">
                  <thead>
                    <th class="col-md-4">Item 1</th>
                    <th class="col-md-2">Qty 1</th>
                    <th class="col-md-4">Item 2</th>
                    <th class="col-md-2">Qty 2</th>
                    <th>&nbsp;</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td><input type="text" name="item1[]" class="form-control" readonly value="Tissue"></td>
                      <td><input type="number" name="qty1[]" class="form-control" min=1 value="1"></td>
                      <td>
                        <select name="item2[]" id="edit-item2" class="form-control">
                          <option value="">Pilih Produk</option>
                          <option value="3">Kertas</option>
                        </select>
                      </td>
                      <td><input type="number" name="qty2[]" class="form-control"></td>
                      <td class="text-center"><button type="button" class="btn btn-warning btn-remove" id="btn-add"><i class="fa fa-times" aria-hidden="true"></i></button></td>
                    </tr>
                  </tbody>
                </table> -->
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
              <button id="delete-productp" type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
              <button id="edit-productp" type="button" class="btn btn-primary">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php echo $footer ?>

  <!-- Control Sidebar -->
  <?php echo $sidebar_control ?>
</div>
<!-- ./wrapper -->

<?php echo $scripts; ?>

<script type="text/javascript">
  "use strict";
  $(document).ready(function($) {

    let pform = $('#pform').validate({
      rules: {
        pname: {
          required : true
        },
        pprice: {
          required: true
        },
        punit: {
          required: true
        },
        ptype: {
          required: true
        }
      },errorPlacement: function(label, element) {
        label.addClass('help-block');
        label.insertAfter(element);
      },
      wrapper: 'span',
      submitHandler: function(form) {

        let url = "<?php echo base_url('Inventory/addproductp'); ?>";
        let data = $('#pform').serialize();

        let success = (data)=>{
          if (data.status==1) {
            populateItemList();
            $('#modal-add-product').modal('hide');
            $("#pform")[0].reset();
            pform.resetForm();
          }
        }

        let error = (data)=>{
          alert('Unexpected error');
        }

        postRequest(url,data,success,error);
        return false;
      }
    });

    let editform = $('#editform').validate({
      rules: {
        pname: {
          required : true
        },
        pprice: {
          required: true
        },
        punit: {
          required: true
        },
        ptype: {
          required: true
        }
      },errorPlacement: function(label, element) {
        label.addClass('help-block');
        label.insertAfter(element);
      },
      wrapper: 'span',
      submitHandler: function(form) {

        let url = "<?php echo base_url('Inventory/editproductp'); ?>";
        let data = $('#editform').serialize();

        let success = (data)=>{
          if (data.status==1) {
            // populateItemList();
            $('#modal-edit-product').modal('hide');
            $("#editform")[0].reset();
            editform.resetForm();
          }
        }

        let error = (data)=>{
          alert('Unexpected error');
        }

        postRequest(url,data,success,error);
        return false;
      }
    });

    $('#add-product').click(function () {
      $('#modal-add-product').modal();
    })

    $('#add-product-convert').click(function () {
      console.log($('#modal-add-product').find('#convert-table').find('tbody'));
      let row = `
      <tr>
        <!--<td><input type="text" name="item1[]" class="form-control" readonly value=""></td>-->
        <td><input type="number" name="qty1[]" class="form-control" min=1 value="1"></td>
        <td>
          <select name="item2[]" id="edit-item2" class="form-control select2">
            <option value="">Pilih Produk</option>
            <?php foreach($product AS $p) { ?>
            <option value="<?php echo $p->id; ?>"><?php echo $p->name; ?></option>
            <?php } ?>
          </select>
        </td>
        <td><input type="number" name="qty2[]" class="form-control"></td>
        <td class="text-center"><button type="button" class="btn btn-warning btn-remove" id="btn-add"><i class="fa fa-times" aria-hidden="true"></i></button></td>
      </tr>
      `;
      $('#modal-add-product').find('#convert-table').find('tbody').append(row);
      $('.btn-remove').click(function () {
        $(this).parents('tr').remove();
      })
      $('.select2').select2();
    })

    $('#add-productp').click(function () {
      $('#pform').submit();
    })

    $('#edit-productp').click(function () {
      $('#editform').submit();
    });

    $('#delete-productp').click(function () {
      if (confirm('Yakin hapus produk?')) {
        let id_item = $('#pid').val();

        let url = "<?php echo base_url('Inventory/deleteproductp'); ?>";
        let data = {
          id_item : id_item
        };

        let success = (data)=>{
          if (data.status==1) {
            populateItemList();
            $('#modal-edit-product').modal('hide');
          }
        }

        let error = (data)=>{
          alert('Unexpected error');
        }

        getRequest(url,data,success,error);
      }
    });

    populateItemList();

    loading();

    function populateItemList() {

      let url = "<?php echo base_url('Inventory/getproduct'); ?>";
      let data = {};
      
      let success = (data)=>{
        console.log(data);
        $("#product-table").dataTable().fnDestroy();
        $('#product-list').empty();
        if (!data.message){
          $('#product-table').DataTable({
            responsive: true,
            paginate: true,
            "columnDefs": [
              {"orderable": false, "targets": [0] }
            ],
            "aaSorting": []
          });
          loading();
          return;
        }

        for (var i = 0; i < data.message.length; i++) {
          let x = data.message[i];
          let type = x.type==1?'DISTRIBUTOR':'AGEN';
          let row = `
          <tr>
            <td>`+(i+1)+`</td>
            <td>`+x.name+`</td>
            <td>`+x.defaultp+`</td>
            <td>`+x.unit+`</td>
            <td>`+type+`</td>            
            <td>`+x.desc+`</td>
            <td class="text-center" >              
              <button id_item=`+x.id+` name="`+x.name+`" defaultp=`+x.defaultp+` unit=`+x.unit+` type=`+x.type+` desc="`+x.desc+`"   type="button" class="btn btn-primary edit-product"><i class="fa fa-edit" aria-hidden="true"></i></button>
            </td>
          </tr>
          `;

          $('#product-list').append(row);
        }

        $('.edit-product').click(function () {
          let modal = $('#modal-edit-product');
          modal.find('#pid').val($(this).attr('id_item'));
          modal.find('#pname').val($(this).attr('name'));
          modal.find('#pprice').val($(this).attr('defaultp'));
          modal.find('#punit').val($(this).attr('unit'));
          modal.find('#pstock').val($(this).attr('stock'));
          modal.find('#ptype').val($(this).attr('type'));
          modal.find('#pdesc').val($(this).attr('desc'));
          $('#modal-edit-product').modal();
        });

        $('#product-table').DataTable({
          responsive: true,
          paginate: true,
          "columnDefs": [
            {"orderable": false, "targets": [0] }
          ],
          "aaSorting": []
        });

        loading();
      }

      let error = (data)=>{
        console.log(data);
        alert('Unknown Error');
        loading();
      }

      loading();
      getRequest(url, data, success, error);
    }

    function loading() {
      $('#loadingSpinner').toggle();
    }

    function getRequest(url, data, success, error) {
      $.ajax({
        url : url,
        data : data,
        type : 'GET',
        success : success,
        error : error
      });
    }

    function postRequest(url, data, success, error) {
      $.ajax({
        url : url,
        data : data,
        type : 'POST',
        success : success,
        error : error
      });
    }

  })
</script>
</body>
</html>