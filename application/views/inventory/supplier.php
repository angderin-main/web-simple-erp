<!DOCTYPE html>
<html>
<head>
  <?php echo $head; ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <?php echo $header; ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php echo $sidebar ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gudang
        <small>Supplier</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i>Gudang</a></li>
        <li class="active">Supplier</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-11">
          <div class="col-md-3">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Menu</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <button id="add-supplier" class="btn btn-primary btn-block" ><i class="fa fa-plus" aria-hidden="true"></i> Tambah supplier</button>
                <!-- <br>
                <a href="<?php echo base_url('addstock') ?>"><button id="add-stock" class="btn btn-primary btn-block" ><i class="fa fa-plus" aria-hidden="true"></i> Tambah stock</button></a> -->
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
          <div class="col-md-9">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Tabel supplier</h3>
                <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <!-- <span class="label label-primary">Label</span> -->
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <table id="supplier-table" class="table table-bordered table-striped" style="width:100%">
                  <thead>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Telepon</th>
                    <th class="col-md-1">Menu</th>
                  </thead>
                  <tbody id="supplier-list">
                  </tbody>
                </table>
              </div><!-- /.box-body -->
              <div id="loadingSpinner" class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div><!-- /.box -->
          </div>
        </div>
      </div>
      <div class="modal fade" id="modal-add-supplier">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Tambah supplier baru</h4>
            </div>
            <div class="modal-body">
              <form id="sform" class="form-horizontal" role="form">
                <div class="form-group">
                  <label for="sname" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input name="sname" type="text" class="form-control" id="sname" placeholder="Nama">
                  </div>
                </div>
                <div class="form-group">
                  <label for="saddress" class="col-sm-2 control-label">Alamat</label>
                  <div class="col-sm-10">
                    <input name="saddress" type="text" class="form-control" id="saddress" placeholder="Alamat">
                  </div>
                </div>
                <div class="form-group">
                  <label for="sphone" class="col-sm-2 control-label">Telepon</label>
                  <div class="col-sm-10">
                    <input name="sphone" type="text" class="form-control" id="sphone" placeholder="Telepon">
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
              <button id="add-supplierp" type="button" class="btn btn-primary">Tambah supplier</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <div class="modal fade" id="modal-edit-supplier">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Edit Supplier</h4>
            </div>
            <div class="modal-body">
              <form id="editform" class="form-horizontal" role="form">
                <input name="sid" type="hidden" id="sid">
                <div class="form-group">
                  <label for="sname" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input name="sname" type="text" class="form-control" id="sname" placeholder="Nama">
                  </div>
                </div>
                <div class="form-group">
                  <label for="saddress" class="col-sm-2 control-label">Alamat</label>
                  <div class="col-sm-10">
                    <input name="saddress" type="text" class="form-control" id="saddress" placeholder="Alamat">
                  </div>
                </div>
                <div class="form-group">
                  <label for="sphone" class="col-sm-2 control-label">Telepon</label>
                  <div class="col-sm-10">
                    <input name="sphone" type="text" class="form-control" id="sphone" placeholder="Telepon">
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
              <!-- <button id="delete-supplierp" type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button> -->
              <button id="edit-supplierp" type="button" class="btn btn-primary">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php echo $footer ?>

  <!-- Control Sidebar -->
  <?php echo $sidebar_control ?>
</div>
<!-- ./wrapper -->

<?php echo $scripts; ?>

<script type="text/javascript">
  $(document).ready(function($) {

    let pform = $('#sform').validate({
      rules: {
        sname: {
          required : true
        }
      },errorPlacement: function(label, element) {
        label.addClass('help-block');
        label.insertAfter(element);
      },
      wrapper: 'span',
      submitHandler: function(form) {

        let url = "<?php echo base_url('Inventory/addsupplierp'); ?>";
        let data = $('#sform').serialize();

        let success = (data)=>{
          if (data.status==1) {
            populateItemList();
            $('#modal-edit-supplier').modal('hide');
            $("#editform")[0].reset();
            pform.resetForm();
          }
        }

        let error = (data)=>{
          alert('Unexpected error');
        }

        postRequest(url,data,success,error);
        return false;
      }
    });

    let editform = $('#editform').validate({
      rules: {
        sname: {
          required : true
        }
      },errorPlacement: function(label, element) {
        label.addClass('help-block');
        label.insertAfter(element);
      },
      wrapper: 'span',
      submitHandler: function(form) {

        let url = "<?php echo base_url('Inventory/editsupplierp'); ?>";
        let data = $('#editform').serialize();

        let success = (data)=>{
          if (data.status==1) {
            populateItemList();
            $('#modal-edit-supplier').modal('hide');
            $("#editform")[0].reset();
            pform.resetForm();
          }
        }

        let error = (data)=>{
          alert('Unexpected error');
        }

        postRequest(url,data,success,error);
        return false;
      }
    });

    $('#add-supplier').click(function () {
      $('#modal-add-supplier').modal();
    });

    $('#add-supplierp').click(function () {
      $('#sform').submit();
    });

    $('#edit-supplierp').click(function () {
      $('#editform').submit();
    });

    $('#delete-supplierp').click(function () {
      if (confirm('Yakin hapus produk?')) {
        let id_item = $('#pid').val();

        let url = "<?php echo base_url('Inventory/deleteproductp'); ?>";
        let data = {
          id_item : id_item
        };

        let success = (data)=>{
          if (data.status==1) {
            populateItemList();
            $('#modal-edit-product').modal('hide');
          }
        }

        let error = (data)=>{
          alert('Unexpected error');
        }

        getRequest(url,data,success,error);
      }
    });

    populateItemList();

    loading();

    function populateItemList() {

      let url = "<?php echo base_url('Inventory/getsupplierp'); ?>";
      let data = {};
      
      let success = (data)=>{
        console.log(data);
        $("#supplier-table").dataTable().fnDestroy();
        $('#supplier-list').empty();
        if (!data.message){
          $('#product-table').DataTable({
            responsive: true,
            paginate: true,
            "columnDefs": [
              {"orderable": false, "targets": [0] }
            ],
            "aaSorting": []
          });
          loading();
          return;
        }

        for (var i = 0; i < data.message.length; i++) {
          let x = data.message[i];
          let type = x.type==1?'DISTRIBUTOR':'AGEN';
          let row = `
          <tr>
            <td>`+(i+1)+`</td>
            <td>`+x.name+`</td>
            <td>`+x.address+`</td>
            <td>`+x.phone+`</td>
            <td class="text-center" >
              <button sid=`+x.id+` name="`+x.name+`" address="`+x.address+`" phone="`+x.phone+`" type="button" class="btn btn-primary edit-supplier"><i class="fa fa-edit" aria-hidden="true"></i></button>
            </td>
          </tr>
          `;

          $('#supplier-list').append(row);
        }

        $('.edit-supplier').click(function () {
          let modal = $('#modal-edit-supplier');
          let supplier = $(this);
          console.log(supplier.attr('name'));
          modal.find('#sid').val(supplier.attr('sid'));
          modal.find('#sname').val(supplier.attr('name'));
          modal.find('#sphone').val(supplier.attr('phone'));
          modal.find('#saddress').val(supplier.attr('address'));
          $('#modal-edit-supplier').modal();
        });

        $('#supplier-table').DataTable({
          responsive: true,
          paginate: true,
          "columnDefs": [
            {"orderable": false, "targets": [0] }
          ],
          "aaSorting": []
        });

        // $('.p-delete').click(function () {
        //   if (confirm('Yakin hapus produk?')) {
        //     let id_item = $(this).attr('id_item')

        //     let url = "<?php echo base_url('Inventory/deleteproductp'); ?>";
        //     let data = {
        //       id_item : id_item
        //     };

        //     let success = (data)=>{
        //       if (data.status==1) {
        //         populateItemList();
        //       }
        //     }

        //     let error = (data)=>{
        //       alert('Unexpected error');
        //     }

        //     getRequest(url,data,success,error);
        //   }
        // })

        

        loading();
      }

      let error = (data)=>{
        console.log(data);
        alert('Unknown Error');
        loading();
      }

      loading();
      getRequest(url, data, success, error);
    }

    function loading() {
      $('#loadingSpinner').toggle();
    }

    function getRequest(url, data, success, error) {
      $.ajax({
        url : url,
        data : data,
        type : 'GET',
        success : success,
        error : error
      });
    }

    function postRequest(url, data, success, error) {
      $.ajax({
        url : url,
        data : data,
        type : 'POST',
        success : success,
        error : error
      });
    }

  })
</script>
</body>
</html>