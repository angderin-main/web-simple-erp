<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_model extends CI_Model {

    public function addstock($spid, $name, $address, $phone, $note, $record)
    {
        $code = "";
        $user = $this->session->userdata('user');

        $stock = array(
            'code' => $code,
            'createdby' => $user['id'],
            'supplier_id' => $spid,
            'supplier_name' => $name,
            'supplier_address' => $address,
            'supplier_phone' => $phone,
            'note' => $note,
            'createdat' => date('Y-m-d H:i:s')
        );

        $this->db->insert('purchase_stock', $stock);

        $purcid = $this->db->insert_id();
        foreach ($record as $key => $r) {
			$this->addstockrecord($purcid, $r['pid'],$r['qty'], $r['subtotal']);
        }
        
        return true;
    }

    public function getstock($pid = null)
    {
        $this->db->select('ps.id, ps.code, u.fullname AS createdby, ps.supplier_id, ps.supplier_name, ps.supplier_address, ps.supplier_phone, ps.note, ps.createdat');
        $this->db->from('purchase_stock AS ps');
        $this->db->join('users AS u', 'u.id = ps.createdby', 'left');
        if(!is_null($pid)){
            $this->db->where('ps.id', $pid);
            $stock = $this->db->get()->row();
        }else{
            $stock = $this->db->get()->result();
        }

        return $stock;
    }

    public function getstockrecord($purcid)
    {
    $this->db->select('psr.id, psr.purcid, psr.pname, psr.punit, psr.pdesc, psr.start, psr.in, psr.final, psr.subtotal');
        $this->db->from('purchase_stock_record AS psr');
        $this->db->where('psr.purcid', $purcid);
        $stock = $this->db->get()->result();

        return $stock;
    }

    public function addstockrecord($purcid, $pid, $in, $subtotal)
    {
        $this->load->model('Inventory_model','inventory');
        $product = $this->inventory->getstock($pid);
        $record = array(
            'purcid' => $purcid,
            'pid' => $pid,
            'pname' => $product->name,
            'punit' => $product->unit,
            'pdesc' => $product->desc,
            'subtotal' => $subtotal,
            'start' => $product->stock,
            'in' => $in,
            'final' => $product->stock+$in
        );

        $this->db->insert('purchase_stock_record', $record);
        $outlet = '001';
        $this->inventory->addstock($outlet,$pid,$in);
    }

}

/* End of file Purchase_model.php */
