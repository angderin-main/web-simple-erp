<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_model extends CI_Model {

    private function generateCode($outlet)
    {
        $ctr = "00001";
		$yymm = date('ym');
		$sql = "SELECT code FROM sales WHERE code LIKE 'INV/$outlet/$yymm/%' ORDER BY code DESC LIMIT 1;";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$lastID = $query->row()->code;
			$temp = explode('/', $lastID);
			$ctr = $temp[3] + 1;
			$ctr = str_pad($ctr, 5, "0", STR_PAD_LEFT);
		}
		// var_dump($ctr); die;
		$purchaseid = "INV/$outlet/$yymm/" . $ctr;
		return $purchaseid;
    }

    public function getomsetdaily($type,$month,$year)
    {
        $d=cal_days_in_month(CAL_GREGORIAN,10,2005);
        $sql = "SELECT DAY(s.createdt) AS `date`, SUM(sp.amount) AS amount FROM sales AS s
                JOIN sales_payment AS sp ON sp.sales_id = s.id
                WHERE s.`type` = '$type'
                AND MONTH(s.createdt) = '$month'
                AND YEAR(s.createdt) = '$year'
                GROUP BY DAY(s.createdt);";
        $query = $this->db->query($sql);
        $result = $query->result();
        
        return $result;
    }

    public function getsalesdetail($sid)
    {
        $this->db->select('s.id, s.createdby, u.fullname AS employee, s.code, s.customer_name, s.customer_phone, s.customer_address, s.createdt, s.total, s.disc, s.type, s.customer_paid');
        $this->db->from('sales AS s');
        $this->db->join('users AS u', 'u.id = s.createdby', 'left');
        $this->db->where('s.id', $sid);
        return $this->db->get()->row();
    }

    public function getsalesproduct($sid)
    {
        $this->db->select('item_name, item_unit, item_type, item_desc, item_dprice, item_aprice, item_fprice, item_qty');
        $this->db->from('sales_product');
        $this->db->where('sales_id', $sid);
        return $this->db->get()->result();
        // return $this->db->get()->row();
    }

    public function getsalespayment($sid)
    {
        $this->db->select('sales.id, customer_paid');
        $this->db->from('sales');
        $this->db->where('id', $sid);
        $sales = $this->db->get()->row();

        return $sales;
    }

    public function payment_in($sid, $receiver, $courier, $amount)
    {
        $payment = array(
            'sales_id' => $sid,
            'created' => date('Y-m-d'),
            'amount' => $amount,
            'receiver' => $receiver,
            'courier' => $courier
        );

        if($this->db->insert('sales_payment', $payment)){
            $sales = $this->getsalespayment($sid);
            $final = $sales->customer_paid+$amount;
            $this->db->set('customer_paid',$final);
            $this->db->where('id', $sid);
            $this->db->update('sales');

            return true;
        }
        
    }

    public function product_in()
    {
        
    }

    public function sale_in($cid, $cname, $cphone, $caddress, $pay, $total, $items, $type, $disc)
    {
        $this->load->helper('json_helper');
        $json = init_json();

        $user = $this->session->userdata('user');

        $this->db->trans_start();

        if ($cid!=0) {
            $this->db->select('*');
            $this->db->from('customer');
            $this->db->where('id', $cid);
            $customer = $this->db->get()->row();
        } else {
            $customer = (object)array(
                'name' => $cname,
                'phone' => $cphone,
                'address' => $caddress
            );                
        }

        $receiver = $this->session->userdata('user')['fullname'];

        $code = $this->generateCode('001');

        $sales = array(
            'code' => $code,
            'createdt' => date('Y-m-d H:i:s'),
            'createdby' => $user['id'],
            'type' => $type,
            'customer_name' => $customer->name,
            'customer_phone' => $customer->phone,
            'customer_address' => $customer->address,
            // 'customer_paid' => $pay,
            'disc' => $disc,
            'total' => $total
        );
        $this->db->insert('sales', $sales);

        $sid = $this->db->insert_id();
        $this->db->set('code',$code);
        $this->db->where('id', $sid);
        $this->db->update('sales');

        $sales_product = array();
        $stock = array();
        $stock_record = array();
        foreach ($items as $i) {
            $temp = array(
                'sales_id' => $sid,
                'item_id' => $i['id'],
                'item_name' => $i['name'],
                'item_unit' => $i['options']['unit'],
                'item_type' => $i['options']['type'],
                'item_desc' => $i['options']['desc'],
                'item_dprice' => $i['options']['dprice'],
                'item_aprice' => $i['options']['aprice'],
                'item_fprice' => $i['price'],
                'item_qty' => $i['qty']
            );
            $sales_product[] = $temp;
            
            $this->db->select('id, stock');
            $this->db->from('stock');
            $this->db->where('pid', $i['id']);
            $oldstock = $this->db->get()->row();

            $temp2 = array(
                'pid' => $i['id'],
                'stock' => ($oldstock->stock-$i['qty']),
                'updatedat' => date('Y-m-d H:i:s')
            );
            $stock = $temp2;
            $this->db->update('stock', $stock, array('id' => $oldstock->id));

            $temp3 = array(
                'createdby' => $user['id'],
                'pid' => $i['id'],
                'start' => $oldstock->stock,
                'in' => 0,
                'out' => $i['qty'],
                'final' => $oldstock->stock - $i['qty'],
                'note' => "",
                'createdat' => date('Y-m-d H:i:s')
            );
            $stock_record[] = $temp3;
        }
        $this->db->insert_batch('sales_product', $sales_product);
        $this->db->insert_batch('stock_record', $stock_record);
        if($pay!=0)$this->payment_in($sid,$receiver,$cname,$pay);

        $message = array();
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $message['status'] = false;
            // generate an error... or use the log_message() function to log your error
            // $json['message'] = log_message();
            // $json['status'] = 0;
        }else{
            $message['status'] = true;
            $message['code'] = str_replace("/","-",$code);
            // $json['message']['notif'] = "Penjualan sukses";
            // $json['message']['sid'] = $sid;
            // $json['status'] = 1;
        }

        return $message;
    }

}

/* End of file Sales_model.php */