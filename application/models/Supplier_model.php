<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier_model extends CI_Model {

    public function getsupplier()
    {
        $this->db->select('sp.id, sp.name, sp.address, sp.phone');
		$this->db->from('supplier AS sp');

		$supplier = $this->db->get()->result();

        return $supplier;
    }

}

/* End of file Supplier_model.php */
