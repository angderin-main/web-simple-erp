<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory_model extends CI_Model {

    /*
    =================START-PRODUCT===================
    */

    public function getproduct($pid=NULL)
    {
		$this->db->select("p.id, p.name, p.defaultp, p.unit, p.type, p.desc");
		$this->db->from('product AS p');
        $this->db->group_by('p.id');
        if(is_null($pid)){
            if($results = $this->db->get()->result()) {
                return $results;
            }
            if($result = $this->db->get()->row()){
                return $result;
            }
        }
		
        return false;
    }

    public function addproduct($name,$price,$unit,$type,$desc,$outlet,$stock)
    {
		$product = array(
			'name' => $name,
			'defaultp' => $price,
			'unit' => $unit,
			'type' => $type,
			'desc' => $desc,
			'created' => date("Y-m-d")
			);
		if ($this->db->insert('product', $product)) {
            $this->newstock($outlet,$this->db->insert_id(),$stock);
			return true;
        }

        return false;
    }

    public function editproeduct($id,$name,$price,$unit,$type,$desc,$outlet,$stock)
    {
		$product = array(
			'name' => $name,
			'defaultp' => $price,
			'unit' => $unit,
			'type' => $type,
			'desc' => $desc,
			'created' => date("Y-m-d")
            );
        $this->db->where('id',$id);
        $this->db->update('product', $product);

        if($this->db->affected_rows()>0){
            // $this->newstock($outlet,$id,$stock);
            return true;
        }

        return false;
    }

    /*
    =================END-PRODUCT===================
    */


    /*
    =================START-STOCK===================
    */

    public function getstock($pid=NULL)
    {
		$this->db->select('p.id, p.name, p.unit, p.type, p.desc, s.stock');
		$this->db->from('product AS p');
        $this->db->join('stock AS s', 's.pid = p.id', 'left');
        $this->db->where('s.outlet', '001');

		if(!is_null($pid)){
            $this->db->where('p.id', $pid);
            $stocks = $this->db->get()->row();
        }
		else{
            $stocks = $this->db->get()->result();
            // foreach ($stocks as $key => $s) {
            //     if($s->stock == null) {$s->stock = 0;}
            // }
        }

        // echo $this->db->last_query();

        return $stocks;
    }

    public function newstock($outlet, $pid, $qty)
    {
		$newstock = array(
			'outlet' => $outlet,
			'pid' => $pid,
			'stock' => $qty,
			'createdat' => date("Y-m-d H:i:s"),
			'updatedat' => date("Y-m-d H:i:s")
			);
		if ($this->db->insert('stock', $newstock)) {
			return true;
        }

        return false;
    }

    public function addstock($outlet, $pid, $qty)
    {        
        $oldstock = $this->getstock($pid);

        $stock = $oldstock->stock + $qty;

		$newstock = array(
			'outlet' => $outlet,
			'pid' => $pid,
			'stock' => $stock,
			'updatedat' => date("Y-m-d H:i:s")
			);
		if ($this->db->update('stock', $newstock, array('pid' => $pid, 'outlet' => $outlet))) {
            $note = "TEST INPUT VIA CONTROLLER";
            if($this->addstockrecord($pid, $oldstock->stock, $qty, 0, $stock, $note)){
                return true;
            }
			
        }
        return false;
    }

    public function removestock($outlet, $pid, $qty)
    {        
        $oldstock = $this->getstock($pid);

        $stock = $oldstock->stock - $qty;
        
		$newstock = array(
			'outlet' => $outlet,
			'pid' => $pid,
			'stock' => $stock,
			'updatedat' => date("Y-m-d H:i:s")
			);
		if ($this->db->update('stock', $newstock, array('pid' => $pid, 'outlet' => $outlet))) {
            $note = "TEST INPUT VIA CONTROLLER";
            if($this->addstockrecord($pid, $oldstock->stock, 0, $qty, $stock, $note)){
                return true;
            }
			
        }
        return false;
    }

    /*
    =================END-STOCK===================
    */

    /*
    =================START-STOCK-RECORD===================
    */

    public function addstockrecord($pid, $start, $in, $out, $final, $note)
    {
        $user = $this->session->userdata('user');

		$record = array(
            'createdby' => $user['id'],
            'pid' => $pid,
            'start' => $start,
            'in' => $in,
            'out' => $out,
            'final' => $final,
            'note' => $note,
			'createdat' => date("Y-m-d H:i:s")
			);
		if ($this->db->insert('stock_record', $record)) {
            return true;
        }

        return false;
    }

    /*
    =================END-STOCK-RECORD===================
    */

    /*
    =================START-SUPPLIER===================
    */

    public function addsupplier($name, $address, $phone)
    {
        $supplier = array(
            'name' => $name,
            'address' => $address,
            'phone' => $phone
			);
		if ($this->db->insert('supplier', $supplier)) {
            return true;
        }
        
        return false;
    }

    public function editsupplier($id, $name, $address, $phone)
    {
        $supplier = array(
            'name' => $name,
            'address' => $address,
            'phone' => $phone
            );
        $this->db->where('id',$id);
        $this->db->update('supplier', $supplier);
		if ($this->db->affected_rows()>0) {
            return true;
        }
        
        return false;
    }
}

/* End of file Inventory_model.php */
