<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		base_auth();
	}
	

	public function index()
	{
		$data = base_view();
		$this->load->view('customer/customerlist', $data, FALSE);
	}

	public function getcustomer()
	{
		$this->load->helper('json_helper');
		$json = init_json();

		$this->db->select('id, name, phone, address');
		$this->db->from('customer');
		if ($results = $this->db->get()->result()) {
			$json['message'] = $results;
			$json['status'] = 1;
		}else{
			$json['message'] = "";
			$json['status'] = 1;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	public function addcustomerp()
	{
		$this->load->helper('json_helper');
		$json = init_json();

		$name = $this->input->post('cname');
		$phone = $this->input->post('cphone');
		$address = $this->input->post('caddress');

		$customer = array(
			'name' => $name,
			'phone' => $phone,
			'address' => $address
			);

		if ($this->db->insert('customer', $customer)) {
			$json['status'] = 1;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	public function editcustomerp()
	{
		$this->load->helper('json_helper');
		$json = init_json();

		$id = $this->input->post('cid');
		$name = $this->input->post('cname');
		$phone = $this->input->post('cphone');
		$address = $this->input->post('caddress');

		$customer = array(
			'name' => $name,
			'phone' => $phone,
			'address' => $address
			);

		$this->db->where('id', $id);
		$this->db->update('customer', $customer);

		if ($this->db->affected_rows()>0) {
			$json['status'] = 1;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	public function deletecustomerp()
	{
		$this->load->helper('json_helper');
		$json = init_json();

		$this->db->where('id', $this->input->post('id'));
		$this->db->delete('customer');
		if ($this->db->affected_rows()>0) {
			$this->db->where('customer_id', $this->input->get('id_item'));
			$this->db->delete('customer_cprice');
			$json['status'] = 1;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	public function customprice()
	{
		$cid = $this->input->get('cid');
		$this->db->select('name');
		$this->db->from('customer');
		$this->db->where('id', $cid);
		$row = $this->db->get()->row();
		$cname = $row->name;
		$data = base_view();
		$data['cid'] = $cid;
		$data['cname'] = $cname;
		$this->load->view('customer/cprice', $data, FALSE);
	}

	public function editcpricep()
	{
		$cpid = $this->input->post('cpid');
		$cprice = $this->input->post('cprice');
		$this->db->set('custom_price',$cprice);
		$this->db->where('id', $cpid);
		$this->db->update('customer_cprice');
		$json['message'] = "";
		$json['status'] = 1;

		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	public function getcprice()
	{
		$this->load->helper('json_helper');
		$json = init_json();

		$cid = $this->input->get('cid');

		$this->db->select('customer_cprice.id AS cpid, product_id AS pid, product.name AS name, defaultp, custom_price, unit, type, desc');
		$this->db->from('customer_cprice');
		$this->db->join('customer', 'customer.id = customer_cprice.customer_id', 'left');
		$this->db->join('product', 'product.id = customer_cprice.product_id', 'left');
		$this->db->where('customer_id', $cid);
		
		if ($results = $this->db->get()->result()) {
			$json['message'] = $results;
			$json['status'] = 1;
		}else{
			$json['message'] = "";
			$json['status'] = 1;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	public function getproduct()
	{
		$this->load->helper('json_helper');
		$json = init_json();

		$this->db->select('id, name, defaultp, unit, type, desc');
		$this->db->from('product');
		$this->db->where('type','2');
		if ($results = $this->db->get()->result()) {
			$json['message'] = $results;
			$json['status'] = 1;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	public function addcpricep()
	{
		$this->load->helper('json_helper');
		$json = init_json();

		$product_id = $this->input->post('pid');
		$customer_id = $this->input->post('cid');
		$custom_price = $this->input->post('cprice');

		$customer_cprice = array(
			'product_id' => $product_id,
			'customer_id' => $customer_id,
			'custom_price' => $custom_price
			);

		$this->db->select('*');
		$this->db->from('customer_cprice');
		$this->db->where('customer_id', $customer_id);
		$this->db->where('product_id', $product_id);
		$cprice = $this->db->get()->row();
		
		if(is_null($cprice)){
			if ($this->db->insert('customer_cprice', $customer_cprice)) {
				$json['status'] = 1;
			}
		}else{
			$this->db->where('customer_id', $customer_id);
			$this->db->where('product_id', $product_id);
			if ($this->db->update('customer_cprice', $customer_cprice)) {
				$json['status'] = 1;
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	public function deletecpricep()
	{
		$this->load->helper('json_helper');
		$json = init_json();
		// $json['status'] = 1;
		// var_dump($json);

		$id = $this->input->post('id');

		$this->db->where('id', $id);
		$this->db->delete('customer_cprice');

		if($this->db->affected_rows()>0){
			$json['status'] = 1;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}
}

/* End of file Customer.php */
/* Location: ./application/controllers/Customer.php */