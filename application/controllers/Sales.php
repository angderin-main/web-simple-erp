<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        base_auth();
    }
    

    public function index()
    {
        $this->load->helper('page_helper');
		$data = base_view();
		$this->load->view('sales/index', $data, FALSE);
    }

    /*
    HISTORY SECTION START
    */

    public function history()
    {
        $this->load->helper('page_helper');
        $data = base_view();

		$this->load->view('sales/history', $data, FALSE);
    }

    public function getSales()
    {
        $this->load->helper('json_helper');
        $data = init_json();

        $this->db->select('sales.id, code, users.fullname AS `createdby`, sales.createdt, customer_name, customer_phone, customer_address, customer_paid, disc, total');
        $this->db->from('sales');
        $this->db->join('users', 'users.id = sales.createdby', 'left');
        $this->db->order_by('sales.createdt', 'desc');
        $sales = $this->db->get()->result();

        $json['message'] = $sales;
        $json['status'] = $this->session->all_userdata();

        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    public function historydetail($code)
    {
        $this->load->helper('page_helper');
        $data = base_view();
        
        $rcode = str_replace("-","/",$code);
        $this->db->select('id');
        $this->db->where('code', $rcode);
        $this->db->from('sales');
        $sales = $this->db->get()->row();
        
        $data['sid'] = $sales->id;
        $data['code'] = $code;

		$this->load->view('sales/history-detail', $data, FALSE);
    }

    public function getSalesproduct()
    {
        $this->load->model('Sales_model','sm');
        $this->load->helper('json_helper');
        $data = init_json();

        $sid = $this->input->get('sid');

        $sales['product'] = $this->sm->getsalesproduct($sid);

        $json['message'] = $sales;
        $json['status'] = 1;

        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    public function getSalesdetail()
    {
        $this->load->model('Sales_model','sm');
        $this->load->helper('json_helper');
        $data = init_json();

        $sid = $this->input->get('sid');

        $sales['sales'] = $this->sm->getSalesdetail($sid);

        $json['message'] = $sales;
        $json['status'] = 1;

        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    public function getSalespayment()
    {
        $this->load->helper('json_helper');
        $data = init_json();

        $sid = $this->input->get('sid');
        
        $this->db->select('created, amount, receiver, courier');
        $this->db->from('sales_payment');
        $this->db->where('sales_id', $sid);
        $sales['payment'] = $this->db->get()->result();

        $json['message'] = $sales;
        $json['status'] = 1;

        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    public function addSalespaymentp()
    {
        $this->load->helper('json_helper');
		$json = init_json();

		$this->load->model('Sales_model','sales');
		$sales_id  = $this->input->post('sid');
        $amount  = $this->input->post('amount');
        $receiver  = $this->session->userdata('user')['fullname'];
        $courier = $this->input->post('courier');
		if($this->sales->payment_in($sales_id,$receiver,$courier,$amount)){
			$json['status'] = 1;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    /*
    HISTORY SECTION END
    */

    public function pricelist()
    {
        $this->load->helper('page_helper');
        $data = base_view();
        
        $this->db->select('*');
        $this->db->from('customer');
        $data['customer'] = $this->db->get()->result();

		$this->load->view('sales/pricelist', $data, FALSE);
    }

    /*
    CASHIER SECTION START
    */

    public function cashier()
    {
        $this->load->helper('page_helper');
        $data = base_view();
        
        $this->db->select('*');
        $this->db->from('customer');
        $data['customer'] = $this->db->get()->result();

		$this->load->view('sales/cashier', $data, FALSE);
    }

    public function getProduct()
    {
        $this->load->helper('json_helper');
        $data = init_json();

        $cid = $this->input->get('cid');
        $type = $this->input->get('type');
        // $cid = 1;

        $this->db->select('p.id, p.name, p.type, p.defaultp as price, p.unit, p.desc, p.desc, s.stock');
        $this->db->from('product AS p');
        if($type!=0){$this->db->where('p.type', $type);}
        $this->db->join('stock AS s', 's.pid = p.id', 'left');
        // $this->db->group_by('pid');
        $products = $this->db->get()->result_array();

        $temp = array();
        foreach ($products as $p) {
            $temp[$p['id']] = $p;
        }

        // var_dump($temp);

        if($cid!=0){
            $this->db->select('*');
            $this->db->from('customer_cprice');
            $this->db->where('customer_id', $cid);
            $cprice = $this->db->get()->result();

            foreach ($cprice as $cp) {
                $temp[$cp->product_id]['price'] = $cp->custom_price;
            }
        }

        // var_dump($temp);
        // die();

        $temp2 = array();
        foreach ($temp as $t) {
            $temp2[] = $t;
        }
        $temp = $temp2;

        $json['message'] = $temp;
        $json['status'] = $this->session->all_userdata();

        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    /*
    CASHIER SECTION END
    */

    /*
    CART SECTION START
    */

    public function addCart()
    {
        $this->load->helper('json_helper');
        $json = init_json();

        // $cid = 1;
        // $id = 2;
        // $qty = 5;
        $cid = $this->input->post('cid');
        $id = $this->input->post('pid');
        $prc = $this->input->post('pprc');
        $qty = $this->input->post('pqty');

        $this->db->select('p.id, p.name, p.defaultp as price, p.unit, p.type, p.desc, SUM(s.stock) as stock');
        $this->db->from('product AS p');
        $this->db->where('p.id', $id);
        $this->db->join('stock AS s', 's.pid = p.id', 'left');
        $this->db->group_by('pid');
        $product = $this->db->get()->row_array();

        // if($product['stock']<$qty){
        //     $json['message'] = "Stock Tidak Mencukupi";
        //     $json['status'] = 0;
        //     $this->output->set_content_type('application/json')->set_output(json_encode($json));
        //     return;
        // }

        // var_dump($temp);
        $product['dprice'] = $product['price'];
        $this->db->select('*');
        $this->db->from('customer_cprice');
        $this->db->where('customer_id', $cid);
        $this->db->where('product_id', $id);
        if ($cprice = $this->db->get()->row()) {
            $product['price'] = $cprice->custom_price;
        }

        $this->load->library('cart');
        $data = array(
            'id'      => $id,
            'qty'     => $qty,
            'price'   => $prc,
            'name'    => $product['name'],
            'options' => array(
                'dprice' => $product['dprice'],
                'aprice' => $prc,
                'unit' => $product['unit'],
                'type' => $product['type'],
                'desc' => $product['desc']
            )
        );

        $prev_rowid = null;
        $prev_item = null;

        foreach ($this->cart->contents() as $item) {
            if($item['id'] == $data['id']){
                $prev_rowid = $item['rowid'];
                $prev_item = $item;
            }
        }

        if (!is_null($prev_rowid)) {
            // $temp = array('rowid' => $prev_rowid, 'qty' => $qty, 'price' => $prc);
            $prev_item['rowid'] = $prev_rowid;
            $prev_item['qty'] = $qty;
            $prev_item['price'] = $prc;
            $prev_item['options']['aprice'] = $prc;
            if ($this->cart->update($prev_item)) {
                $json['message'] = "Keranjang Berhasil di Update";
                $json['status'] = 1;
            }else{
                $json['message'] = "Gagal Mengupdate Ke Keranjang 1";
                $json['status'] = 0;
            }
        }else{
            if($this->cart->insert($data)){
                $json['message'] = "Keranjang Berhasil di Tambahkan";
                $json['status'] = 1;
            }else{
                $json['message'] = "Gagal Menambahkan Ke Keranjang 2";
                $json['status'] = 0;
            }
        }

        $this->applyDisc();

        $this->output->set_content_type('application/json')->set_output(json_encode($json));
        

        // $this->cart->destroy();

        // var_dump($this->cart->contents());
        
    }

    public function getCart()
    {
        $this->load->helper('json_helper');
        $json = init_json();
        
        $this->load->library('cart');
        foreach ($this->cart->contents() as $x) {
            $json['message']['cart'][] = $x;
        }

        // var_dump($this->session->all_userdata());

        $json['message']['total'] = $this->cart->total();

        $this->applyDisc();

        $this->output->set_content_type('application/json')->set_output(json_encode($json));        
    }

    public function clearCart()
    {
        $this->load->helper('json_helper');
        $json = init_json();
        
        $this->load->library('cart');
        $this->cart->destroy();
        
        $this->session->unset_userdata('disc');
        
        $json['message'] = "Sukses menghapus cart";
        $json['status'] = 1;

        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    public function removeItem()
    {
        $this->load->helper('json_helper');
        $json = init_json();

        $rowid = $this->input->post('rowid');
        
        $this->load->library('cart');
        if ($this->cart->remove($rowid)) {
            $json['message'] = "Sukses Menghapus Item";
            $json['status'] = 1;
        }else{
            $json['message'] = "";
            $json['status'] = 0;
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    public function setDisc()
    {
        $disc = $this->input->post('disc');
        // $disc = 0.05;
        
        $array = array(
            'disc' => $disc
        );
        
        $this->session->set_userdata( $array );      
        
        $this->applyDisc();
    }

    public function applyDisc()
    {
        $this->load->helper('json_helper');
        $json = init_json();

        // $disc = $this->input->post('disc');
        
        if($this->session->userdata('disc')!=NULL){
            $disc = $this->session->userdata('disc');
        }
        else{
            $array = array(
                'disc' => 0
            );
            
            $this->session->set_userdata( $array );
            $disc = 0;
        }

        // $disc = 0.05;
        
        $this->load->library('cart');
        $cart = $this->cart->contents();

        $affectedproduct = array();

        foreach ($cart as $key => $c) {
            if($c['options']['type']==1){

                // $this->db->select('*');
                // $this->db->from('product');
                // $this->db->where('id', $c['id']);
                // $product = $this->db->get()->row();

                // $newprice = $product->defaultp * (1-$disc);
                $newprice = $c['options']['aprice'] * (1-$disc);
                
                $data = array(
                    'rowid' => $c['rowid'],
                    'price' => $newprice
                );
                
                $this->cart->update($data);

                $affectedproduct[] = $c['name'];
                $json['status'] = 1;
            }
        }

        $json['message'] = $affectedproduct;

        // $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    /*
    CART SECTION END
    */

    public function salep()
    {
        $this->load->helper('json_helper');
        $json = init_json();
        
        $this->load->library('cart');
        $items = $this->cart->contents();
        $total = $this->cart->total();
        $cid = $this->input->post('cid');
        $pay = $this->input->post('pay');
        $cname = $this->input->post('cname');
        $cphone = $this->input->post('cphone');
        $caddress = $this->input->post('caddress');
        $type = $this->input->post('type');
        if($type=="2"){
            $disc = 0;
        } else {
            $disc = $this->session->userdata('disc');
        }

        $this->load->model('Sales_model','sales');
        $message = $this->sales->sale_in($cid,$cname,$cphone,$caddress,$pay,$total,$items,$type,$disc);

        if($message['status']){
            $json['message']['notif'] = "Penjualan sukses";
            $json['message']['code'] = $message['code'];
            // $json['message']['code'] = $;
            $json['status'] = 1;
        }else{
            // $json['message'] = log_message();
            $json['status'] = 0;
        }

        // if(true){
        //     $this->db->trans_start();

        //     if ($cid!=0) {
        //         $this->db->select('*');
        //         $this->db->from('customer');
        //         $this->db->where('id', $cid);
        //         $customer = $this->db->get()->row();
        //     } else {
        //         $customer = (object)array(
        //             'name' => $cname,
        //             'phone' => $cphone,
        //             'address' => $caddress
        //         );                
        //     }
    
        //     $sales = array(
        //         'createdt' => date('Y-m-d H:i:s'),
        //         'code' => "",
        //         'customer_name' => $customer->name,
        //         'customer_phone' => $customer->phone,
        //         'customer_address' => $customer->address,
        //         'customer_paid' => $pay,
        //         'total' => $total
        //     );
        //     $this->db->insert('sales', $sales);
    
        //     $sid = $this->db->insert_id();
        //     $sales_product = array();
        //     $stock = array();
        //     foreach ($items as $i) {
        //         $temp = array(
        //             'sales_id' => $sid,
        //             'item_id' => $i['id'],
        //             'item_name' => $i['name'],
        //             'item_unit' => $i['options']['unit'],
        //             'item_type' => $i['options']['type'],
        //             'item_desc' => $i['options']['desc'],
        //             'item_dprice' => $i['options']['dprice'],
        //             'item_fprice' => $i['price'],
        //             'item_qty' => $i['qty']
        //         );
        //         $sales_product[] = $temp;
        //         $temp2 = array(
        //             'product_id' => $i['id'],
        //             'stock' => (0-$i['qty']),
        //             'updated' => date('Y-m-d H:i:s')
        //         );
        //         $stock[] = $temp2;
        //     }
        //     $this->db->insert_batch('sales_product', $sales_product);
        //     $this->db->insert_batch('stock', $stock);

        //     $this->db->trans_complete();
        //     if ($this->db->trans_status() === FALSE)
        //     {
        //         // generate an error... or use the log_message() function to log your error
        //         $json['message'] = log_message();
        //         $json['status'] = 0;
        //     }else{
        //         $json['message']['notif'] = "Penjualan sukses";
        //         $json['message']['sid'] = $sid;
        //         $json['status'] = 1;
        //     }
        // }else{
        //     $json['message'] = "Pembayaran kurang";
        //     $json['status'] = 0;
        // }

        // var_dump($json);

        // echo json_decode($json)
        // echo json_encode($json);

        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

}

/* End of file Sales.php */
/* Location: ./application/controllers/Sales.php */