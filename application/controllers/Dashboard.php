<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		base_auth();
	}

	public function index()
	{
		if($this->session->userdata('user')['id']>=3){redirect('cashier');}

		$this->load->model('Sales_model','sm');

		$month = date('m');
		// $month = "10";
		$year = date('Y');
		// $d=cal_days_in_month(CAL_GREGORIAN,$month,$year);
		$d = date('d');
		// $d = 31

		$result = $this->sm->getomsetdaily("1",$month,$year);

		$omset1 = array();
		$totalomset1 = 0;

		for ($i=0; $i < $d; $i++) { 
			$omset1[$i] = 0;
		}

		foreach ($result as $key => $r) {
			$omset1[$r->date] = $r->amount;
			$totalomset1+=$r->amount;
		}

		$result = $this->sm->getomsetdaily("2",$month,$year);

		$omset2 = array();
		$totalomset2 = 0;
		for ($i=0; $i < $d; $i++) { 
			$omset2[$i] = 0;
		}

		foreach ($result as $key => $r) {
			$omset2[$r->date] = $r->amount;
			$totalomset2+=$r->amount;
		}

		// var_dump($omset1);
		// die();
		
		$this->load->helper('page_helper');
		$data = base_view();
		$data['omset1'] = $omset1;
		$data['omset2'] = $omset2;
		$data['totalomset1'] = $totalomset1;
		$data['totalomset2'] = $totalomset2;
		$this->load->view('dashboard/index', $data, FALSE);
	}

	

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */