<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

define('DOMPDF_ENABLE_AUTOLOAD', false);

require_once('./application/libraries/dompdf/autoload.inc.php');

require_once './application/libraries/dompdf/lib/html5lib/Parser.php';

require_once './application/libraries/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';

require_once './application/libraries/dompdf/lib/php-svg-lib/src/autoload.php';

require_once './application/libraries/dompdf/src/Autoloader.php';



use Dompdf\Dompdf;

use Dompdf\Options;

use FontLib\Font;

class Printpdf extends CI_Controller{

	

	public function __construct(){

		parent::__construct();

		$this->load->helper('url');

		// $this->load->model('productmd_model');

	}

	public function generat_dompdf($html, $customPaper,$filename, $stream=true)

	{

		$options = new Options();

		$options->setIsRemoteEnabled(true);

		$dompdf = new DOMPDF($options);

		$dompdf->load_html($html);

		$dompdf->set_paper($customPaper);

		//$dompdf->set_paper($paper, $orientation);

		$dompdf->render();// Render the HTML as PDF

		// Output the generated PDF to Browser

		$canvas = $dompdf->get_canvas();		

		$canvas->page_text(514, 400, "Page: {PAGE_NUM} of {PAGE_COUNT}", '', 10, array(0,0,0));

		$canvas->page_text(35, 400, "Date: ".date('d-m-Y'), '', 10, array(0,0,0));

		if ($stream){

			$dompdf->stream($filename.".pdf", array("Attachment" => false));

		}else{

			$output =  $dompdf->output();

			//file_put_contents('./pdf-printout/'.$filename.".pdf", $output);//save file pdf to server

			return $output;

		}

		exit(0);

	}

	function mutasi($id=''){

		$id = str_replace('-','/',$id);

		$filename = 'Rekap Mutasi #'.$id;

		$sql = $this->query_mutation($id);

		$query = $this->db->query($sql)->result();		

		foreach ($query as $row)

			foreach ($row as $key=>$val){

			$data[$key] = $val;

		}

		$data['products']= $this->get_mutation_detail($id);		

		$data['title'] = $filename;

		$header =  $this->load->view('printpdf/mutasi/header',$data,true);

		$body = $this->load->view('printpdf/mutasi/body',$data,true);	

		$footer = $this->load->view('printpdf/mutasi/footer',$data,true);

		$html = $header.$body.$footer;

		$customPaper = array(0,0,595.275590551,841.88976378); //A4,portrait = 210 mm, 297 mm/ 21,0 cm, 29,7 cm (convert to point)

		$this->generat_dompdf(preg_replace('/>\s+</', "><", $html),$customPaper,$filename);//off break in last page

	}

	function query_mutation($id){

		$sql = "SELECT m.id, m.note, o1.name AS 'fromname', o2.name AS 'toname',

		m.courierdt, m.receivedt,

		e1.name AS 'createdbyname', e2.name AS 'couriername', e3.name AS 'receivername'

		FROM mutation m JOIN employee e1 ON m.createdby = e1.id

		JOIN employee e2 ON m.courier = e2.id

		LEFT JOIN employee e3 ON m.receiver = e3.id

		JOIN outlet o1 ON m.from  = o1.code

		JOIN outlet o2 ON m.to  = o2.code

		WHERE m.id = '$id';";

		return $sql;

	}

	

	function get_mutation_detail($id){		

		$sql = $this->db->select('product,productcode')->from('mutation_product')->where('mutationid',$id)->get();

		foreach ($sql->result() as $row){

			$this->db->distinct();

			$this->db->select('B.product,B.productcode');

			$this->db->select('C.label');

			if ($row->product == 'DJ'){

				$this->db->select('A.code,A.img,A.ocode,A.origin,A.weightm + (A.weightg/5) as weight');				

				$this->db->from('productdj as A');

			}else{

				$this->db->select('A.code,A.certimg as img,A.ocode, A.origin, A.weight');			

				$this->db->from('productls as A');

			}			

			$this->db->join('mutation_product as B','B.productcode = A.code','left');

			$this->db->join('subspecies as C','A.sspc = C.code','left');

			$this->db->where('A.code',$row->productcode);

			$sql	=  $this->db->get()->result();

			$data[] = $sql;

		}

		return $data;

	}

	function mdmutasi($id=''){

		$id = str_replace('-','/',$id);

		$filename = 'Rekap Merchandise Mutasi #'.$id;

		$query = $this->productmd_model->getMdMutation(array('A.id'=>$id))->result();

		foreach ($query as $row)

			foreach ($row as $key=>$val){

				$data[$key] = $val;

		}

		$data['products']= $this->productmd_model->getMdDetail(array('pmd.mutationid'=>$id))->result();

		$data['title'] = $filename;

		$header =  $this->load->view('printpdf/mutasi/header',$data,true);

		$body = $this->load->view('printpdf/mutasi/mdbody',$data,true);

		$footer = $this->load->view('printpdf/mutasi/footer',$data,true);

		$html = $header.$body.$footer;

		$customPaper = array(0,0,595.275590551,841.88976378); //A4,portrait = 210 mm, 297 mm/ 21,0 cm, 29,7 cm (convert to point)

		$this->generat_dompdf(preg_replace('/>\s+</', "><", $html),$customPaper,$filename);//off break in last page

    }
    
    public function invoice($code)
    {
		$this->load->model('Sales_model','sm');
		
		$rcode = str_replace('-','/',$code);

        $this->db->select('id');
        $this->db->where('code', $rcode);
        $this->db->from('sales');
        $sales = $this->db->get()->row();

		$filename = 'Invoice #'.$code;

		// $this->db->select('*');
		// $this->db->from('sales');
		// $this->db->where('code', $code);
		// $query = $this->db->get()->row();
		$detail = $this->sm->getsalesdetail($sales->id);
		$products = $this->sm->getsalesproduct($sales->id);

		// var_dump($products);
		// die();

		foreach ($detail as $key => $val) {

			$data[$key] = $val;

		}

		$data['products'] = $products;

		// $data['products']= $this->productmd_model->getMdDetail(array('pmd.mutationid'=>$id))->result();

		$data['title'] = $filename;

		$header_html = $this->load->view('printpdf/header-html',$data,true);

		$footer_html = $this->load->view('printpdf/footer-html',$data,true);

		$size = count($products);
		$isFirstpage = true;
		$final_body = "";
		$max = 0;
		$counter = 0;
		$start = 0;
		$end = 0;
		$total = 0;
		$dtotal = 0;
		$data['size'] = $size;
		if($size>=12){
			
			while ($counter!=$size) {
				$final_product = array();
				if($counter>=($size-12))$isFirstpage = false;
				if($isFirstpage){

					$data['counter'] = $counter;
					$max = 20;
					$start = $counter;
					$end = $counter+$max;
					$data['max'] = $max;
					for ($counter=$start; $counter < $end; $counter++) { 
						$product = $products[$counter];
						$final_product[] = $product;
						$total += $product->item_fprice*$product->item_qty;
						$dtotal += $product->item_aprice*$product->item_qty;
						$end-=(ceil(strlen($product->item_name)/34)*1);
					}
					$data['total'] = $total;
					$data['dtotal'] = $dtotal;
					$data['products'] = $final_product;
					// var_dump($end);
					// die();
					$header =  $this->load->view('printpdf/header',$data,true);
					$body = $this->load->view('printpdf/body2',$data,true);
					$footer = $this->load->view('printpdf/footer',$data,true);
	
					$final_body = $final_body.$header.$body.$footer;
					// $isFirstpage = false;
				} else {
	
					$data['counter'] = $counter;
					$max = 12;
					$start = $counter;
					$end = $counter+$max;
					$data['max'] = $max;
					for ($counter=$start; $counter < $end; $counter++) { 
						if(isset($products[$counter])){
							$product = $products[$counter];
							$final_product[] = $product;
							$total += $product->item_fprice*$product->item_qty;
							$dtotal+=$product->item_aprice*$product->item_qty;
						} else {
							break;
						}
					}
					$data['dtotal'] = $dtotal;
					$data['total'] = $total;
					$data['products'] = $final_product;
					$header =  $this->load->view('printpdf/header',$data,true);
					$body = $this->load->view('printpdf/body2',$data,true);
					$footer = $this->load->view('printpdf/footer2',$data,true);
	
					$final_body = $final_body.$header.$body.$footer;
				}
			}			
			
		} else {
			$max = 10;
			$data['max'] = $max;
			$data['products'] = $products;
			foreach ($products as $key => $product) {
				$total += $product->item_fprice*$product->item_qty;
				$dtotal += $product->item_aprice*$product->item_qty;
			}
			$data['total'] = $total;
			$data['dtotal'] = $dtotal;
			$header =  $this->load->view('printpdf/header',$data,true);
			$body = $this->load->view('printpdf/body',$data,true);
			$footer = $this->load->view('printpdf/footer3',$data,true);

			$final_body = $final_body.$header.$body.$footer;
		}

		$html = $header_html.$final_body.$footer_html;
		// $html = $html.$html;

		// var_dump($data);
		// die();

		// echo $html;
		// die();

		// $customPaper = array(0,0,595.275590551,841.88976378); //A4,portrait = 210 mm, 297 mm/ 21,0 cm, 29,7 cm (convert to point)
		$customPaper = array(0,0,598.96063,396); //A4,portrait = 210 mm, 297 mm/ 21,0 cm, 29,7 cm (convert to point)

		$this->generat_dompdf(preg_replace('/>\s+</', "><", $html),$customPaper,$filename);//off break in last page
    }

}