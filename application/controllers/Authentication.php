<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		
	}

	public function index()
	{
		if ($this->session->userdata('login')) {
			redirect('Dashboard','refresh');
		}

		$data = base_view();
		$this->load->view('authentication/index', $data, FALSE);
	}	

	public function loginp()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$this->db->select('id,fullname');
		$this->db->from('users');
		$this->db->where('username', $username);
		$this->db->where('password', md5($password));

		if ($user = $this->db->get()->row_array()) {

			$session = array(
				'user' => $user,
				'login' => TRUE
				);

			$currdt = date("Y-m-d h:i:s");
			$user['logindt'] = $currdt;
			// $this->db->update('users', $user);
			
			$this->session->set_userdata( $session );
			redirect('','refresh');
		}else{
			redirect('login','refresh');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login','refresh');
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */