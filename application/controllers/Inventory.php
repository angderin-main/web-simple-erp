<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {

	public function index()
	{
		
	}

	public function product()
	{
		$this->load->model('Inventory_model','im');
		$data = base_view();
		$data['product'] = $this->im->getproduct();
		$this->load->view('inventory/product', $data, FALSE);
	}

	public function getproduct()
	{
		$this->load->helper('json_helper');
		$json = init_json();

		$this->load->model('Inventory_model','inventory');
		$json['message'] = $this->inventory->getproduct();
		$json['status'] = 1;

		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	public function editproductp()
	{
		$this->load->helper('json_helper');
		$json = init_json();

		$this->load->model('Inventory_model','inventory');
		$pid = $this->input->post('pid');
		$newname  = $this->input->post('pname');
		$newprice = $this->input->post('pprice');
		$newunit  = $this->input->post('punit');
		$newqty   = $this->input->post('pqty');
		$newtype  = $this->input->post('ptype');
		$newdesc  = $this->input->post('pdesc');
		$newoutlet = '001';
		$newstock = $this->input->post('pstock');
		if($this->inventory->editproeduct($pid,$newname,$newprice,$newunit,$newtype,$newdesc,$newoutlet,$newstock)){
			$json['status'] = 1;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	public function addproductp()
	{
		$this->load->helper('json_helper');
		$json = init_json();

		$this->load->model('Inventory_model','inventory');
		$name  = $this->input->post('pname');
		$price = $this->input->post('pprice');
		$unit  = $this->input->post('punit');
		$qty   = $this->input->post('pqty');
		$type  = $this->input->post('ptype');
		$desc  = $this->input->post('pdesc');
		$outlet = '001';
		$stock = $this->input->post('pstock');
		
		if($this->inventory->addproduct($name,$price,$unit,$type,$desc,$outlet,$stock)){
			$json['status'] = 1;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	public function deleteproductp()
	{
		$this->load->helper('json_helper');
		$json = init_json();

		$this->db->where('id', $this->input->get('id_item'));
		$this->db->delete('product');
		if ($this->db->affected_rows()>0) {
			$this->db->where('product_id', $this->input->get('id_item'));
			$this->db->delete('customer_cprice');
			$json['status'] = 1;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	/*
	STOCK SECTION START
	*/

	public function stock()
	{
		$data = base_view();
		$this->load->view('inventory/stock', $data, FALSE);
	}
	
	public function getstockp()
	{
		$this->load->helper('json_helper');
		$json = init_json();

		$this->load->model('Inventory_model','inventory');
		$json['message'] = $this->inventory->getstock();
		$json['status'] = 1;

		// var_dump($json);
		
		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	public function addstock(Type $var = null)
	{
		$data = base_view();
		$this->load->model('Supplier_model','supplier');
		$this->load->model('Inventory_model','inventory');
		$data['supplier'] = $this->supplier->getsupplier();
		$data['stock'] = $this->inventory->getstock();
		$this->load->view('inventory/addstock', $data, FALSE);
	}

	public function addstockp()
	{
		$spid = $this->input->post('spid');
		$name = $this->input->post('spname');
		$address = $this->input->post('spaddress');
		$phone = $this->input->post('spphone');
		$pid = $this->input->post('pid');
		$qty = $this->input->post('qty');
		$subtotal = $this->input->post('subtotal');
		$note = $this->input->post('note');

		// var_dump($this->input->post());
		// die();
		$stock = array();
		foreach ($pid as $key => $p) {
			$stock[] = array('pid' => $pid[$key], 'qty' => $qty[$key], 'subtotal' => $subtotal[$key]);
		}

		$this->load->model('Purchase_model','purchase');
		$purcid = $this->purchase->addstock(1,$name, $address, $phone, $note, $stock);

		
		redirect('stock','refresh');
		

		// foreach ($pid as $key => $p) {
		// 	$this->purchase->addstockrecord($purcid, $pid[$key],$qty[$key]);
		// }
		
		// var_dump($pid);
		// var_dump($qty);

		// $outlet = '001';
		// $pid = 3;
		// $qty = 10;

		// $this->load->model('Inventory_model','inventory');
		// $this->inventory->removestock($outlet,$pid,$qty);
	}

	/*
	STOCK SECTION END
	*/

	public function supplier()
	{
		$data = base_view();
		$this->load->view('inventory/supplier', $data, FALSE);
	}

	public function getsupplierp()
	{
		$this->load->helper('json_helper');
		$json = init_json();

		$this->db->select('id, name, address, phone');
		$this->db->from('supplier');
		$suppliers = $this->db->get()->result();

		$json['message'] = $suppliers;
		$json['status'] = 1;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	public function addsupplierp()
	{
		$this->load->helper('json_helper');
		$json = init_json();

		$this->load->model('Inventory_model','inventory');
		$name  = $this->input->post('sname');
		$address = $this->input->post('saddress');
		$phone  = $this->input->post('sphone');
		if($this->inventory->addsupplier($name,$address,$phone)){
			$json['status'] = 1;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	public function editsupplierp()
	{
		$this->load->helper('json_helper');
		$json = init_json();

		$this->load->model('Inventory_model','inventory');
		$id = $this->input->post('sid');
		$name  = $this->input->post('sname');
		$address = $this->input->post('saddress');
		$phone  = $this->input->post('sphone');
		if($this->inventory->editsupplier($id,$name,$address,$phone)){
			$json['status'] = 1;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	public function convert()
	{
		$data = base_view();
		$this->load->view('inventory/convert', $data, FALSE);
	}

	public function addconvertp()
	{
		$item1 = $this->input->post('item1');
		$qty1 = $this->input->post('qty1');
		$item2 = $this->input->post('item2');
		$qty2 = $this->input->post('qty2');
		
		$newconvert = array(
			'item1' => $item1,
			'qty1' => $qty1,
			'item2' => $item2,
			'qty2' => $qty2
		);

		$this->db->insert('convert', $newconvert);
	}

	public function editconvertp()
	{
		$id = $this->input->post('id');
		$item1 = $this->input->post('item1');
		$qty1 = $this->input->post('qty1');
		$item2 = $this->input->post('item2');
		$qty2 = $this->input->post('qty2');
		
		$newconvert = array(
			'item1' => $item1,
			'qty1' => $qty1,
			'item2' => $item2,
			'qty2' => $qty2
		);

		$this->db->where('id', $id);
		$this->db->update('convert', $newconvert);
	}

	public function deleteconvertp()
	{
		$id = $this->input->post('id');

		$this->db->where('id', $id);
		$this->db->delete('convert');
	}

	public function getconvertproduct()
	{
		$sql = "SELECT p.name FROM product AS p
				JOIN `convert` AS c ON c.item1 <> p.id;";
		$query = $this->db->query($sql);

		$result = $query->result();
	}
}
		

/* End of file Inventory.php */
/* Location: ./application/controllers/Inventory.php */