<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase extends CI_Controller {

    public function index()
    {
        
    }

    public function stock()
	{
		$data = base_view();
		$this->load->view('purchase/stock', $data, FALSE);
    }
    
    public function getstockp()
    {
        $this->load->helper('json_helper');
		$json = init_json();

		$this->load->model('Purchase_model','purchase');
		$json['message'] = $this->purchase->getstock();
		$json['status'] = 1;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    public function stockrecord($pid)
    {
        $data = base_view();

        $data['pid'] = $pid;

		$this->load->view('purchase/stock-record', $data, FALSE);
    }

    public function getstockdetailp()
    {
        $this->load->helper('json_helper');
		$json = init_json();

        $pid = $this->input->get('pid');
		$this->load->model('Purchase_model','purchase');
		$json['message'] = $this->purchase->getstock($pid);
		$json['status'] = 1;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    public function getstockproductp()
    {
        $this->load->helper('json_helper');
		$json = init_json();

        $pid = $this->input->get('pid');
		$this->load->model('Purchase_model','purchase');
		$json['message'] = $this->purchase->getstockrecord($pid);
		$json['status'] = 1;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

}

/* End of file Purchase.php */
