<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {

    public function index()
    {
        
    }

    public function view($id)
    {
        echo $this->generate($id);
    }

    public function generate($id)
    {
        $this->db->select('*');
        $this->db->from('sales');
        $this->db->where('id', $id);
        $s = $this->db->get()->row();

        $this->db->select('*');
        $this->db->from('sales_product');
        $this->db->where('sales_id', $s->id);
        $sp = $this->db->get()->result();

        $data['s'] = $s;
        $data['sp'] = $sp;

        // $this->load->view('invoice/invoice_template', $data, FALSE);
        $invoice = $this->load->view('invoice/invoice_template', $data, TRUE);

        return $invoice;
    }

}

/* End of file Invoice.php */
