<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Dashboard/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['invoice/(:any)'] = 'Invoice/view/$1';

$route['loginp'] = 'Authentication/loginp';
$route['login'] = 'Authentication/index';
$route['logout'] = 'Authentication/logout';

$route['product'] = 'Inventory/product';
$route['stock'] = 'Inventory/stock';
$route['addstock'] = 'Inventory/addstock';
$route['supplier'] = 'Inventory/supplier';

$route['customer-price'] = 'Customer/customprice';
$route['customer'] = 'Customer/index';

$route['history'] = 'Sales/history';
$route['detail/(:any)'] = 'Sales/historydetail/$1';
$route['cashier'] = 'Sales/cashier';
$route['pricelist'] = 'Sales/pricelist';

$route['purchasestock'] = 'Purchase/stock';
$route['purchasestockdetail/(:any)'] = 'Purchase/stockrecord/$1';