<?php
	if (!function_exists("base_view")) {
		function base_view()
		{
			$CI = &get_instance();
			$data['head'] = $CI->load->view('fix_templates/head', NULL, TRUE);
			$data['header'] = $CI->load->view('fix_templates/header', NULL, TRUE);
			$data['sidebar'] = $CI->load->view('fix_templates/sidebar', NULL, TRUE);
			$data['footer'] = $CI->load->view('fix_templates/footer', NULL, TRUE);
			$data['sidebar_control'] = $CI->load->view('fix_templates/sidebar_control', NULL, TRUE);
			$data['scripts'] = $CI->load->view('fix_templates/scripts', NULL, TRUE);
			return $data;
		}
	}
?>