<?php

if (!function_exists("base_auth")) {
	function base_auth()
	{
		$CI = &get_instance();

		if (!$CI->session->userdata('login')) {
			$CI->session->sess_destroy();
			redirect('login','refresh');
		}
	}
}

?>